# How to compile
Compilation of libgcms, liblabid and miami was tested on Ubuntu 18.04. The instructions should work mit Ubuntu based systems with apt installed.

## Install dependencies

To compile the programs, compilation tools and ccmake are needed:
`sudo apt install build-essential cmake-curses-gui`.

Install the needed boost libraries:
`sudo apt install libboost1.65-dev libboost-filesystem1.65-dev libboost-system1.65-dev libboost-regex1.65-dev`.

Install all other needed libraries:
`sudo apt install zlib1g-dev libgsl-dev libnetcdf-dev libsqlite3-dev`.

## Library libgcms
Go to the gcms source folder. Create a build folder and change the directory:
`mkdir build && cd build`

Call `ccmake ..` and configure the build with `c`, `c` (the default values are fine). If no errors are thrown, generate the build files with `g` and exit ccmake with `q`.

Now call `make` to compile libgcms. Install the library when the process has finished: `sudo make install`.

## liblabid
Go to the labid source folder. Create a build folder and change the directory:
`mkdir build && cd build`

Call `ccmake ..` and configure the build with `c`, `c` (the default values are fine). If no errors are thrown, generate the build files with `g` and exit ccmake with `q`.

Now call `make` to compile liblabid. Install the library when the process has finished: `sudo make install`.

## miami
Go to the miami source folder. Create a build folder and change the directory:
`mkdir build && cd build`

Call `ccmake ..` and configure the build with `c`, `c`, if you want to create the source code documentation, toggle the corresponding entry. If no errors are thrown, generate the build files with `g` and exit ccmake with `q`.

Now call `make` to compile miami. The executable will be generated in the build/src/ folder.
