# Use miami
The miami executable can be distributed and executed elsewhere, but needs some dependencies to be fulfilled before usage.

## Install dependencies
Install bosst-regex and libgsl: `sudo apt install libboost-regex1.65.1 libgsl23`

# Usage {#UsageExecution}
To create a new configuration file with default options use `miami_suite -c <filename>`, where filename should end with `.json`. Edit `<filename>` with a text editor and fill in your data (See [Configuration](@ref UsageConfiguration)). Run the program with the created configuration with `miami_suite -r <config_file> -o <output_file>`. The `<output_file>` will be in `.json` format, if no output file was given, the results will be written to the configuration file.

# Configuration {#UsageConfiguration}
The configuration is split into two sections.

## Parameter configuration
The configuration section handles all parameters for the data processing pipeline. Most parameters have a default value, except library and reference pathway files.

- `identification_cutoff`
    (float) Minimum identification score during metabolite identification (default: `0.7`)
- `labid_dev_abs_sum`
    (float) The maximum deviation from 1.0 of the sum of mass isotopomers during label detection (default: `0.2`)
- `labid_max_labeling`
    (float) Percent maximum labeling for label detection. `0.5` = 50% maximum labeling. Should be set to the amount of expected maximum labeling from the experiment (default: `0.5`)
- `labid_min_labeling`
    (float) Percent minimum labeling for label detection. `0.05` = 5% minimum labeling. (default: `0.05`)
- `labid_min_midsolv_r2`
    (float) Minimum R^2 from MID determination during label detection (default: `0.95`)
- `labid_required_replicates`
    (float) Fractions of replicates needed for label detection. `1.0` detects only metabolites which are found in all replicates, with `0.5` a metabolite needs to be found in at least 50% of the replicates. (default: `1.0`)
- `libraries`
    (list with strings) List of paths to files with libraries in MetaboliteDetector format (`.lbr`) (default: `[]`)
- `library_nist`
    (string) Path to the NIST library file imported with MetaboliteDetector. (default: `""`)
- `nist_overwrite`
    (boolean) If `true`, a NIST hit better than all other identification hits will overwrite the identification. This will also delete a previously assigned KEGG Id! (default: `false`)
- `library_match_cutoff`
    (float) Minimum identification score for matching similar metabolites during targeted library creation (default: `0.6`)
- `miami_error_per_isotopomer`
    (float) Error tolerance from 1.0 for the absolute sum of isotopomers during ion selection. A metabolite with 6 carbon atoms would have an error tolerance of 6% (default: `0.01`)
- `miami_distance_cutoff`
    (float) Starting distance cut off for the visualization (default: `0.1`)
- `miami_variability_cutoff`
    (float) Starting variability cut off for the visualization (default: `0.05`)
- `miami_distance_method`
    (string) Method for distance calculation, valid values are: `Euclidean`, `Canberra`, `Manhattan` (default: `"Euclidean"`, see [here](@ref miami_tools::Distance) for details)
- `miami_distance_norm`
    (string) Method for distance normalization, valid values are: `None`, `Sum`, `Prod`, `Max`, `Min` (default: `"Sum"`, see [here](@ref miami_tools::Normalization) for details)
- `mid_identification_cutoff`
    (float) Minimum identification score during targeted search (default: `0.6`)
- `reference_files`
    (list with strings) List of paths to files with reference pathways, absolute or relative paths will work, but absolute paths should be used (default: `[]`)
- `reference_full`
    (boolean) If `true` full reference pathways will be used (default: `false`)
- `reference_shortest`
    (boolean) If `true` all paths between metabolites in reference pathways will be used, only used if `reference_full` is `true` (default: `false`)
- `reference_depth`
    (float) Maximum number of reference metabolites between two nodes. If `0` there is no limit. Will be ignored if `reference_full` is `true` (default: `0`)
- `tracer`
    (string) The isotope used as tracer as one letter symbol, used for MID correction (default: `"C"`)

## Experiment configuration
The experiments path defines the name and files for each experiment. For every experiment, a name, list of labeled files and list of unlabeled files need to be given.

- `experiments`
  (list of objects) A list of objects with experiment data. The format is described below (default: `[]`)

Every experiments entry needs to contain:
- `name`
  (string) Name of the experiment
- `labeled_files`
  (list of strings) List of file paths to preprocessed MetaboliteDetector files with labeling (`.cmp`)
- `unlabeled_files`
  (list of strings) List of file paths to preprocessed MetaboliteDetector files with without labeling (`.cmp`)

## Example experiment section
```json
"experiments": [
  {
    "name": "Control",
    "labeled_files": [
      "/path/to/file/ctrl_labeled_1.cmp",
      "/path/to/file/ctrl_labeled_2.cmp"
    ],
    "unlabeled_files": [
      "/path/to/file/ctrl_unlabeled_1.cmp",
      "/path/to/file/ctrl_unlabeled_2.cmp"
    ]
  },
  {
    "name": "Treatment",
    "labeled_files": [
      "/path/to/file/treat_labeled_1.cmp",
      "/path/to/file/treat_labeled_2.cmp"
    ],
    "unlabeled_files": [
      "/path/to/file/treat_unlabeled_1.cmp",
      "/path/to/file/treat_unlabeled_2.cmp"
    ]
  }
]
```
