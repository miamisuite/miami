
/**
 * @brief      Calculates the distance between two vectors.
 *
 * @param[in]  v1                    The first vector of values
 * @param[in]  v2                    The second vector of values
 * @param[in]  distance_method       The distance method
 * @param[in]  normalization_method  The normalization method
 *
 * @return     The distance.
 */
double distance(const std::vector<double>& v1, const std::vector<double>& v2,
                Distance distance_method, Normalization normalization_method) {
  // Align the vectors
  std::pair<std::vector<double>, std::vector<double>> alignment =
      needlemanWunsch(v1, v2);
  std::vector<double> a1 = alignment.first;
  std::vector<double> a2 = alignment.second;

  double distance;

  // Calculate the distance based on given method
  switch (distance_method) {
    case Distance::Euclidean:
      distance = euclideanDistance(a1, a2);
      break;
    case Distance::Manhattan:
      distance = manhattanDistance(a1, a2);
      break;
    default:  // Default distance method: Canberra Distance
      distance = canberraDistance(a1, a2);
      break;
  }

  // Normalize by given method
  switch (normalization_method) {
    case Normalization::Min:
      distance /= std::min(v1.size(), v2.size());
      break;
    case Normalization::Max:
      distance /= std::max(v1.size(), v2.size());
      break;
    case Normalization::Sum:
      distance /= (v1.size() + v2.size());
      break;
    case Normalization::Prod:
      distance /= (v1.size() * v2.size());
      break;
    default:  // No normalization Normalization::None
      break;
  }

  return distance;
}
