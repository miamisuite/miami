/** @file edge.hpp
    @brief MIAMI edge object
    @copyright Copyright (C) 2019 Christian-Alexander Dudek

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EDGE_HPP
#define EDGE_HPP

#include <iostream>
#include <map>

// #include "node.hpp"

#include <nlohmann/json.hpp>
using json = nlohmann::json;

namespace miami {

/**
 * @brief      Class for edge.
 */
class Edge {
 public:
  Edge(int source, int target);
  ~Edge();

  void addExperiment(std::string experiment, double distance);

  void fromJson(json jsonImport);
  json toJson();
  std::string toString();

 private:
  int source_id;
  int target_id;
  std::map<std::string, double> distances;
};
}  // namespace miami

#endif  // EDGE_HPP
