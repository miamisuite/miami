/** @file node.cpp
    @brief MIAMI node object
    @copyright Copyright (C) 2019 Christian-Alexander Dudek

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "node.hpp"
#include "config.hpp"
#include "experiment.hpp"
#include "tools.hpp"

#include <algorithm>
#include <chrono>
#include <iostream>
#include <sstream>
#include <vector>

// Labid headers
#include "labelidentificator.h"

// External headers
#include <nlohmann/json.hpp>
using json = nlohmann::json;

namespace miami {

/**
 * @brief      Constructs the node object.
 */
Node::Node()
    : id(0),
      metabolite_name("Unknown metabolite"),
      reference_metabolite(nullptr),
      rt(-1.0),
      ri(-1.0),
      kegg_id(""),
      is_ref(false),
      largest_common_ion(-1),
      identification_score(-1),
      identification_source("None"),
      labeled_files(0),
      unlabeled_files(0) {}

/**
 * @brief      Constructs the node object.
 *
 * @param[in]  id    The identifier
 */
Node::Node(int id)
    : id(id),
      metabolite_name("Unknown metabolite"),
      reference_metabolite(nullptr),
      rt(-1.0),
      ri(-1.0),
      is_ref(false),
      kegg_id(""),
      largest_common_ion(-1),
      identification_score(-1),
      identification_source("None"),
      labeled_files(0),
      unlabeled_files(0) {}

/**
 * @brief      Constructs the node object.
 *
 * @param[in]  id               The identifier
 * @param[in]  metabolite_name  The metabolite name
 */
Node::Node(int id, std::string metabolite_name)
    : id(id),
      metabolite_name(std::move(metabolite_name)),
      largest_common_ion(-1),
      rt(-1.0),
      ri(-1.0),
      is_ref(false),
      reference_metabolite(nullptr),
      identification_score(-1),
      identification_source("None"),
      labeled_files(0),
      unlabeled_files(0) {}

/**
 * @brief      Destroys the object.
 */
Node::~Node() = default;

/**
 * @brief      Sets if the node is a reference node.
 *
 * @param[in]  is_ref  Indicates if node is a reference node
 */
void Node::setRef(bool is_ref) { this->is_ref = is_ref; }

/**
 * @brief      Determines if node is a reference node.
 *
 * @return     True if reference, False otherwise.
 */
bool Node::isRef() { return this->is_ref; }

/**
 * @brief      Sets the number of files.
 *
 * @param[in]  labeled    The number of labeled files
 * @param[in]  unlabeled  The number of unlabeled files
 */
void Node::setFiles(int labeled, int unlabeled) {
  this->labeled_files = labeled;
  this->unlabeled_files = unlabeled;
}

/**
 * @brief      Sets the retention time.
 *
 * @param[in]  rt    The retention time
 */
void Node::setRt(double rt) { this->rt = rt; }

/**
 * @brief      Sets the retention index.
 *
 * @param[in]  ri    The retention index
 */
void Node::setRi(double ri) { this->ri = ri; }

/**
 * @brief      Sets the kegg id.
 *
 * @param[in]  kegg_id  The kegg id
 */
void Node::setKegg(std::string kegg_id) { this->kegg_id = kegg_id; }

/**
 * @brief      Gets the kegg id.
 *
 * @return  kegg_id  The kegg id
 */
std::string Node::getKegg() { return this->kegg_id; }

/**
 * @brief      Sets the reference metabolite object.
 *
 * @param      metabolite  The compound object of the refrence metabolite
 * @param[in]  score       The identification score
 * @param[in]  source      The identification source
 */
void Node::setReference(gcms::LibraryCompound<int, float>* metabolite,
                        double score, std::string source) {
  this->reference_metabolite = metabolite;
  this->identification_score = score;
  this->identification_source = std::move(source);
  if (metabolite != nullptr) {
    this->rt = metabolite->getRetentionTime();
    this->ri = metabolite->getRetentionIndex();
    this->kegg_id = metabolite->getFeature("PRECURSOR_KEGG_ID");
  }
}

/**
 * @brief      Gets the compound objectof the reference metabolite.
 *
 * @return     The compound object of the reference metabolite.
 */
gcms::LibraryCompound<int, float>* Node::getReference() {
  return this->reference_metabolite;
}

/**
 * @brief      Adds a spectrum to the node.
 *
 * @param      metabolite  The metabolite compund object
 */
void Node::addSpectrum(gcms::LibraryCompound<int, float>* metabolite) {
  this->reference_metabolite->addCompoundSpectrum(*metabolite);
}

/**
 * @brief      Gets the experiments.
 *
 * @return     The experiments.
 */
std::vector<std::string> Node::getExperiments() const {
  std::vector<std::string> tmp;
  for (auto exp : this->mids) {
    tmp.push_back(exp.first);
  }
  return tmp;
}

/**
 * @brief      Gets the node id.
 *
 * @return     The node id.
 */
int Node::getId() const { return this->id; }

/**
 * @brief      Gets the metabolite name.
 *
 * @return     The metabolite name.
 */
std::string Node::getName() const { return this->metabolite_name; }

/**
 * @brief      Sets the metabolite name.
 *
 * @param[in]  metabolite_name  The metabolite name
 */
void Node::setName(std::string metabolite_name) {
  this->metabolite_name = std::move(metabolite_name);
}

// void Node::printMids() {
//   // for (auto exp : this->mids) {
//   //   debug(exp.first) for (auto ion : this->getIons(exp.first)) {
//   //     debug("  " << ion) for (auto m : getMids(exp.first, ion)) {
//   //       debug("    " << m.first << " +- " << m.second);
//   //     }
//   //   }
//   // }
// }

/**
 * @brief      Finds the largest common ion
 *
 * @param[in]  err_per_isotopomer  The error per isotopomer
 */
void Node::findLargestCommonIon(double err_per_isotopomer) {
  std::map<int, int> ion_count;
  this->largest_common_ion = -1;

  double best_score = 1000.0;

  for (auto experiment : this->mids) {
    std::vector<int> ions = this->getIons(experiment.first);
    for (int iIon = ions.size() - 1; iIon >= 0; --iIon) {
      int ion = ions[iIon];

      double score = 0;

      std::vector<std::pair<double, double>> mids =
          this->getMids(experiment.first, ion);

      double score_cutoff = 1 + (mids.size() - 1) * err_per_isotopomer;  //

      for (auto m : mids) {
        score += std::fabs(m.first);
      }

      // Check MID score, only allow score < 1+mid.size()*0.01
      if (score > score_cutoff) {
        continue;
      }

      // If we have not counted this ion before
      if (ion_count.find(ion) == ion_count.end()) {
        ion_count[ion] = 0;
      }

      ion_count[ion]++;

      // Check if we already got experiments.size() number of hits for this ion
      if (ion_count[ion] == this->mids.size()) {
        this->largest_common_ion = ion;
        break;
      }
    }
    // If we have a largest common ion, break the loop
    if (this->largest_common_ion != -1) {
      break;
    }
  }

}

/**
 * @brief      Calculates the variability.
 */
void Node::calculateVariability() {
  std::vector<std::string> experiments = this->getExperiments();
  std::sort(experiments.begin(), experiments.end());
  double pvalue = 0.0;
  for (const auto& exp1 : experiments) {
    for (const auto& exp2 : experiments) {
      // Skip same experiment
      if (exp1 == exp2) {
        continue;
      }

      const std::string& tmp1 = std::min(exp1, exp2);
      const std::string& tmp2 = std::max(exp1, exp2);

      // M0 variability
      // Skip if we already have a value
      if (this->m0_variability.find(tmp1) != this->m0_variability.end() &&
          this->m0_variability[tmp1].find(tmp2) !=
              this->m0_variability[tmp1].end()) {
        continue;
      }
      std::vector<std::pair<double, double>> mid1 =
          this->getMids(tmp1, this->getCommonIon());
      std::vector<std::pair<double, double>> mid2 =
          this->getMids(tmp2, this->getCommonIon());

      // Calculate Pvalue
      pvalue = miami_tools::ttest(mid1[0].first, mid2[0].first, mid1[0].second,
                                  mid2[0].second, this->labeled_files,
                                  this->labeled_files);

      this->m0_variability[tmp1][tmp2] =
          std::pair<double, double>(mid1[0].first - mid2[0].first, pvalue);

      // Mx variability, the maximum variability of the isotopomers
      int mx_isotopomer = 0;
      double max = 0.0;
      for (int i = 1; i < mid1.size(); i++) {
        double tmp = mid1[i].first - mid2[i].first;
        if (std::abs(tmp) > std::abs(max)) {
          max = tmp;
          mx_isotopomer = i;
        }
      }

      // Calculate Pvalue
      int i = mx_isotopomer;
      pvalue = miami_tools::ttest(mid1[i].first, mid2[i].first, mid1[i].second,
                                  mid2[i].second, this->labeled_files,
                                  this->labeled_files);

      this->mx_variability[tmp1][tmp2] =
          std::pair<int, std::pair<double, double>>(
              mx_isotopomer, std::pair<double, double>(max, pvalue));

      // FC variability, variability of fractional contribution
      double fc1 = miami_tools::fractionalContribution(mid1);
      double fc2 = miami_tools::fractionalContribution(mid2);

      this->fc_variability[tmp1][tmp2] =
          std::pair<double, double>(fc1 - fc2, 1.0);
    }
  }
}

/**
 * @brief      Gets the variability between two experiments.
 *
 * @return     The variability matrix.
 */
std::map<std::string, std::map<std::string, std::pair<double, double>>>
Node::getVariability() {
  return this->m0_variability;
}

/**
 * @brief      Gets the M_0 variability between two experiments.
 *
 * @param[in]  exp1  The first experiment
 * @param[in]  exp2  The second experiment
 *
 * @return     The M_0 variability between *exp1* and *exp2*.
 */
std::pair<double, double> Node::getM0Variability(const std::string& exp1,
                                                 const std::string& exp2) {
  return this->m0_variability[std::min(exp1, exp2)][std::max(exp1, exp2)];
}

/**
 * @brief      Gets the maximum variability of all mass isotopomers.
 *
 * @param[in]  exp1  The first experiment
 * @param[in]  exp2  The second experiment
 *
 * @return     The M_x variability between *exp1* and *exp2*.
 */
std::pair<int, std::pair<double, double>> Node::getMxVariability(
    const std::string& exp1, const std::string& exp2) {
  return this->mx_variability[std::min(exp1, exp2)][std::max(exp1, exp2)];
}

/**
 * @brief      Gets the variability of the fractional contribution  between two
 * experiments.
 *
 * @param[in]  exp1  The first experiment
 * @param[in]  exp2  The second experiment
 *
 * @return     The FC variability between *exp1* and *exp2*.
 */
std::pair<double, double> Node::getFcVariability(const std::string& exp1,
                                                 const std::string& exp2) {
  return this->fc_variability[std::min(exp1, exp2)][std::max(exp1, exp2)];
}

/**
 * @brief      Gets the ions for a given experiment.
 *
 * @param[in]  experiment_name  The experiment name
 *
 * @return     The ion vector.
 */
std::vector<int> Node::getIons(const std::string& experiment_name) {
  std::vector<int> tmp;
  for (auto ion : this->getMids(experiment_name)) {
    tmp.emplace_back(ion.first);
  }
  return tmp;
}

/**
 * @brief      Gets all ions.
 *
 * @return     The ions as a map of experiment name and ion vector.
 */
std::map<std::string, std::vector<int>> Node::getIons() {
  std::map<std::string, std::vector<int>> tmp;
  for (auto experiment : this->mids) {
    tmp[experiment.first] = this->getIons(experiment.first);
  }
  return tmp;
}

/**
 * @brief      Gets the largest common ion.
 *
 * @return     The largest common ion.
 */
int Node::getCommonIon() const { return this->largest_common_ion; }

/**
 * @brief      Determines if node is valid.
 *
 * @return     True if valid, False otherwise.
 */
bool Node::isValid() const { return !(this->largest_common_ion == -1); }

/**
 * @brief      Sets the mids.
 *
 * @param[in]  mids  The mids
 */
void Node::setMids(
    std::map<std::string, std::map<int, std::vector<std::pair<double, double>>>>
        mids) {
  this->mids = std::move(mids);
}

void Node::setQuant(std::string group, double mean, double err) {
  this->quant[group] = std::make_pair(mean, err);
}

/**
 * @brief      Gets all mids.
 *
 * @return     The mids.
 */
std::map<std::string, std::map<int, std::vector<std::pair<double, double>>>>
Node::getMids() {
  return this->mids;
}

/**
 * @brief      Gets the mids for a given experiment and ion.
 *
 * @param[in]  experiment_name  The experiment name
 * @param[in]  ion              The ion
 *
 * @return     The mids.
 */
std::vector<std::pair<double, double>> Node::getMids(
    const std::string& experiment_name, int ion) {
  return this->getMids(experiment_name)[ion];
}

/**
 * @brief      Gets all mids for a given experiment.
 *
 * @param[in]  experiment_name  The experiment name
 *
 * @return     The mids.
 */
std::map<int, std::vector<std::pair<double, double>>> Node::getMids(
    const std::string& experiment_name) {
  return this->mids[experiment_name];
}

/**
 * @brief      Gets all mids for a given ion.
 *
 * @param[in]  ion   The ion
 *
 * @return     The mids.
 */
std::map<std::string, std::vector<std::pair<double, double>>> Node::getMids(
    int ion) {
  std::map<std::string, std::vector<std::pair<double, double>>> tmp;
  for (auto exp : this->mids) {
    tmp[exp.first] = this->getMids(exp.first)[ion];
  }
  return tmp;
}

float Node::getMidSum(const std::string& experiment_name, int ion) {
  float sum = 0.0;
  for (auto m : this->getMids(experiment_name)[ion]) {
    sum += m.first;
  }
  return sum;
}

/**
 * @brief      Gets the identification score.
 *
 * @return     The identification score.
 */
double Node::getIdentificationScore() const {
  return this->identification_score;
}

/**
 * @brief      Gets the identification source.
 *
 * @return     The identification source.
 */
std::string Node::getIdentificationSource() const {
  return this->identification_source;
}

/**
 * @brief      Returns a csv representation of the node.
 *
 * @return     csv representation of the object.
 */
std::string Node::toString() {
  std::stringstream ss;
  ss << this->id;
  ss << "," << this->getName();
  ss << "," << reference_metabolite->getName();
  ss << "," << reference_metabolite->getRetentionTime();
  ss << "," << reference_metabolite->getRetentionIndex();
  ss << "," << this->rt;
  ss << "," << this->ri;
  ss << "," << this->getCommonIon();
  if (this->getIdentificationScore() > -1) {
    ss << "," << this->getIdentificationSource();
    ss << "," << this->getIdentificationScore();
  } else {
    ss << ",";
    ss << ",";
  }
  // ss << std::endl;
  // if (this->getCommonIon() != -1) {
  //   for (auto experiment : this->getMids()) {
  //     int n = 0;
  //     ss << "  " << experiment.first;
  //     ss << std::endl;
  //   }
  // }
  // ss << std::endl;
  return ss.str();
}

std::string Node::variabilityToString() {
  std::stringstream ss;

  for (auto const& [exp1, row] : this->m0_variability) {
    ss << exp1 << " ";
    for (auto const& [exp2, var] : row) {
      ss << var.first << " ";
    }
    ss << std::endl;
  }

  return ss.str();
}

/**
 * @brief      Returns a json representation of the node.
 *
 * @return     The node json object.
 */
void Node::fromJson(json jsonImport) {
  this->id = jsonImport["id"];
  this->rt = jsonImport["rt"];
  this->ri = jsonImport["ri"];
  this->metabolite_name = jsonImport["name"];
  this->largest_common_ion = jsonImport["ion"];
  this->identification_source = jsonImport["library"];
  this->identification_score = jsonImport["score"];
  // TODO(cdu): Add MIDs, quantification and variability from config:
  // this->quant_mean = jsonImport["quant"][0];
  // this->quant_err = jsonImport["quant"][1];
  // this->set = jsonImport["mids"];
  // this->m0_variability = jsonImport["variability"];
}

/**
 * @brief      Returns a json representation of the node.
 *
 * @return     The node json object.
 */
json Node::toJson() {
  json jsonExport;
  jsonExport["id"] = this->id;
  jsonExport["rt"] = this->rt;
  jsonExport["ri"] = this->ri;
  jsonExport["kegg_id"] = this->kegg_id;
  jsonExport["name"] = this->metabolite_name;
  jsonExport["ion"] = this->largest_common_ion;
  jsonExport["library"] = this->identification_source;
  jsonExport["score"] = this->identification_score;
  jsonExport["is_ref"] = this->is_ref;
  jsonExport["mids"] = this->getMids(this->largest_common_ion);
  jsonExport["quant"] = this->quant;
  jsonExport["variability"]["m0"] = this->m0_variability;
  jsonExport["variability"]["mx"] = this->mx_variability;
  jsonExport["variability"]["fc"] = this->fc_variability;

  return jsonExport;
}

}  // namespace miami
