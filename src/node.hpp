/** @file node.hpp
    @brief MIAMI node object
    @copyright Copyright (C) 2019 Christian-Alexander Dudek

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NODE_HPP
#define NODE_HPP

#include <iostream>

#include "config.hpp"
#include "experiment.hpp"
#include "librarycompound.h"

#include <nlohmann/json.hpp>
using json = nlohmann::json;

namespace miami {

/**
 * @brief      Class for node.
 */
class Node {
 public:
  Node();
  Node(int id);
  Node(int id, std::string metabolite_name);
  ~Node();

  void setRef(bool is_ref);
  bool isRef();

  int getId() const;

  void setFiles(int labeled, int unlabeled);

  void setName(std::string metabolite_name);
  std::string getName() const;

  std::vector<std::string> getExperiments() const;
  // miami::Experiment* getExperiment(int idx) const;

  // int addMetabolite(miami::Experiment* experiment,
  //                   labid::LabeledCompound* metabolite);

  // void printMids();

  void setRt(double rt);
  void setRi(double ri);
  void setKegg(std::string kegg_id);
  std::string getKegg();
  void setReference(gcms::LibraryCompound<int, float>* metabolite, double score,
                    std::string source);
  gcms::LibraryCompound<int, float>* getReference();

  void addSpectrum(gcms::LibraryCompound<int, float>* metabolite);

  std::vector<int> getIons(const std::string& experiment_name);
  std::map<std::string, std::vector<int>> getIons();

  void setMids(std::map<std::string,
                        std::map<int, std::vector<std::pair<double, double>>>>
                   mids);
  void setQuant(std::string group, double mean, double err);
  std::map<std::string, std::map<int, std::vector<std::pair<double, double>>>>
  getMids();
  std::vector<std::pair<double, double>> getMids(
      const std::string& experiment_name, int ion);
  std::map<int, std::vector<std::pair<double, double>>> getMids(
      const std::string& experiment_name);
  std::map<std::string, std::vector<std::pair<double, double>>> getMids(
      int ion);
  float getMidSum(const std::string& experiment_name, int ion);

  int getCommonIon() const;
  bool isValid() const;

  std::string getIdentificationSource() const;
  double getIdentificationScore() const;

  void findLargestCommonIon(double err_per_isotopomer);
  void calculateVariability();

  std::pair<double, double> getM0Variability(const std::string& exp1,
                                             const std::string& exp2);
  std::pair<int, std::pair<double, double>> getMxVariability(
      const std::string& exp1, const std::string& exp2);
  std::pair<double, double> getFcVariability(const std::string& exp1,
                                             const std::string& exp2);
  std::map<std::string, std::map<std::string, std::pair<double, double>>>
  getVariability();

  std::string variabilityToString();

  void fromJson(json jsonImport);
  json toJson();
  std::string toString();

 private:
  int id;
  int largest_common_ion;
  int labeled_files;
  int unlabeled_files;
  double identification_score;
  std::map<std::string, std::pair<double, double>> quant;
  std::string metabolite_name;
  std::string identification_source;

  gcms::LibraryCompound<int, float>* reference_metabolite;

  std::map<std::string, std::map<int, std::vector<std::pair<double, double>>>>
      mids;
  std::map<std::string, labid::LabeledCompound*> metabolites;

  std::map<std::string, std::map<std::string, std::pair<double, double>>>
      m0_variability;
  std::map<std::string,
           std::map<std::string, std::pair<int, std::pair<double, double>>>>
      mx_variability;
  std::map<std::string, std::map<std::string, std::pair<double, double>>>
      fc_variability;

  std::string kegg_id;
  bool is_ref;
  double rt;
  double ri;
};
}  // namespace miami

#endif  // NODE_HPP
