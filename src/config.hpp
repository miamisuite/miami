/** @file config.hpp
    @brief MIAMI configuration object
    @copyright Copyright (C) 2019 Christian-Alexander Dudek

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CONFIG_H
#define CONFIG_H

#include "tools.hpp"

#include <string>
#include <vector>

// External headers
#include "nlohmann/json.hpp"
using json = nlohmann::json;

namespace miami {
/**
 * @brief      Class for miami configuration.
 */

class Config {
 public:
  Config();
  ~Config();
  size_t hash();
  void addLibrary(const std::string& library_path);
  void removeLibrary(const std::string& library_path);
  void loadJson(const std::string& config_filename);
  void saveJson(const std::string& config_filename);
  void addFile(const std::string& name);
  void addFiles(const std::vector<std::string>& names);
  const std::vector<std::string> getFiles();
  void fromJson(json jsonImport);
  std::vector<std::string> checkFiles();
  json toJson();

  double labid_min_labeling;    /**< Minimum amount of labeling. */
  double labid_max_labeling;    /**< Maximum amount of labeling (should be
                                   determind from experimental design). */
  double labid_min_midsolv_r2;  /**< \f$R^2\f$ cutoff for MID determination. */
  double labid_dev_abs_sum;     /**< +- deviation from 1.0 in \f$\sum_{i=0}^{n}
                                   abs(m_i)\f$ with \f$m_i\f$ is the mass
                                   isotopomer. */
  double library_match_cutoff;  /**< Cutoff score for metabolite matching in
                                   internal library creation. */
  double identification_cutoff; /**< Cutoff score for identification of
                                   metabolites. */
  double mid_identification_cutoff;  /**< Cutoff score for metabolite matching
                                        during MID determination (targeted
                                        search). */
  int miami_node_name; /**< Display name of metabolite nodes */
  double miami_error_per_isotopomer; /**< Allowed deviation from 1.0 for every
                                        mass isotopomer */
  double miami_distance_cutoff;      /**< Distance cutoff for edge creation
                                        (visualization only). */
  double miami_variability_cutoff;   /**< Variability cutoff for change icon
                                        determination (visualization only) */
  int miami_quant_cutoff; /**< Cutoff for quantification (visualization only) */
  bool miami_nodes_over_edges; /**< Defines if nodes are drawn over edges (visualization only) */
  bool miami_single_nodes; /**< Defines if unconnected nodes are shown (visualization only) */
  bool miami_show_arrows; /**< Defines if FC arrows are shown (visualization only) */
  bool miami_show_targets; /**< Defines if targets are shown (visualization only) */
  bool miami_show_references; /**< Defines if reference nodes are shown (visualization only) */
  bool miami_remove_edges; /**< Defines if edges are removed (visualization only) */


  miami_tools::Distance
      miami_distance_method; /**< Method for distance calculation */

  miami_tools::Normalization
      miami_distance_norm; /**< Method for distance normalization */

  double labid_required_replicates; /**< Percent of replicates needed to include
                                       the metabolite durig label detection. 1.0
                                       means the metabolite needs to be found in
                                       all replicates. */
  std::vector<std::string>
      reference_files; /**< Path to reference pathway files */
  int reference_depth; /**< Number of reference metabolites between found
                          metabolites */
  bool reference_full; /**< Defines if all reference metabolites should be used
                        */
  bool reference_shortest; /**< Defines if only the shortest path between
                            * reference metabolites should be used
                            */
  std::vector<std::string> reference_remove; /**< Metabolite names or KEGGids
                                              * to exclude from refrence pathway
                                              */

  std::string library_nist; /**< Path to the NIST SQLite library */
  bool nist_overwrite;      /**< Can NIST identification overwrite other
                               identification? */

  std::string tracer; /**< The labeled molecule used for labeling as one
                         capital letter */

  std::vector<std::string> files;

  /* TODO(cdu): add miami_gap_penalty to config */
  double
      miami_gap_penalty; /**< The gap penalty for Needleman-Wunsch alignment. */
  std::vector<std::string>
      libraries; /**< Path to the libraries for metabolite identification */
  bool verbose = false;

 private:
};
}  // namespace miami

#endif  // CONFIG_H
