/** @file experiment.hpp
    @brief MIAMI experiment object
    @copyright Copyright (C) 2019 Christian-Alexander Dudek

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef EXPERIMENT_HPP
#define EXPERIMENT_HPP

#include "config.hpp"

#include <iostream>

// Labid headers
#include "labeledcompound.h"

// External headers
#include <nlohmann/json.hpp>
using json = nlohmann::json;

namespace miami {
class Node;

/**
 * @brief      Class for experiment.
 */
class Experiment {
 public:
  Experiment(std::string experiment_name, std::map<int, Node*>* nodes,
             std::vector<std::string> labeled_files,
             std::vector<std::string> unlabeled_files, Config* config);
  ~Experiment();

  std::string getName() const;
  void setName(const std::string experiment_name);
  int numLabeledFiles() const;
  int numUnlabeledFiles() const;
  void setLabeledFiles(std::vector<std::string> files);
  void setUnlabeledFiles(std::vector<std::string> files);
  std::vector<std::string> getLabeledFiles() const;
  std::vector<std::string> getUnlabeledFiles() const;
  std::vector<gcms::LibraryCompound<int, float>*> getAllMetabolites() const;
  std::vector<labid::LabeledCompound*> getLabeledMetabolites() const;

  void detectLabeledMetabolites();
  void calculateDistances();
  std::map<int, std::map<int, double>> getDistances();

  std::map<int, Node*>* getNodes() const;

  std::string toString();

  void fromJson(json jsonImport);
  json toJson();

  std::vector<std::string> checkFiles();
  // Node* getNode(int node_id);
  // void addNode(int node_id, Node* node);
  // int delNode(int node_id);

 private:
  Config* config;
  std::string name;
  std::vector<std::string> labeled_files;
  std::vector<std::string> unlabeled_files;
  std::vector<labid::LabeledCompound*> labeled_metabolites;
  std::vector<gcms::LibraryCompound<int, float>*> all_metabolites;
  std::map<int, std::map<int, double>> distance_matrix;
  std::map<int, Node*>* nodes;
};
}  // namespace miami

#endif  // EXPERIMENT_HPP
