/** @file miami_cmd.cpp
    @brief MIAMI main executable
    @copyright Copyright (C) 2019 Christian-Alexander Dudek

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "miami_cmd.hpp"
#include "config.hpp"
#include "experiment.hpp"
#include "miami.hpp"
#include "tools.hpp"

#include <getopt.h>
#include <unistd.h>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

// External headers
#include <nlohmann/json.hpp>
using json = nlohmann::json;

// Macros
#ifdef MIAMI_DEBUG_LEVEL
#define DEBUG 1
#else
#define DEBUG 0
#endif

std::chrono::high_resolution_clock::time_point now() {
  return std::chrono::high_resolution_clock::now();
}

float dt(std::chrono::high_resolution_clock::time_point time_start,
         std::chrono::high_resolution_clock::time_point time_end) {
  return (float)std::chrono::duration_cast<std::chrono::milliseconds>(
             time_end - time_start)
             .count() /
         1000;
}

void print_help() {
  std::cout << "MIAMI v" << MIAMI_VERSION << std::endl
            << "To create default configuration:" << std::endl
            << "    miami -c <configuration_file>" << std::endl
            << "To run an experiment:" << std::endl
            << "    miami -r <configuration_file> [-v] [-o <output_file>]"
            << std::endl;
}

int create_config(const std::string& config_file) {
  std::ofstream ofs;
  json jsonConfig;
  std::vector<int> experiments;  // Empty dummy vector

  auto default_config = new miami::Config();

  jsonConfig["config"] = default_config->toJson();
  jsonConfig["experiments"] = experiments;

  ofs.open(config_file);
  ofs << std::setw(4) << jsonConfig;
  ofs.close();

  return 1;
}

int main(int argc, char* argv[]) {
  bool verbose = false;
  std::string config_file;
  std::string output_file;

  // Load data from command line
  const char* const short_opts = "c:r:o:hvz";
  const option long_opts[] = {{"configure", required_argument, nullptr, 'c'},
                              {"run", required_argument, nullptr, 'r'},
                              {"help", no_argument, nullptr, 'h'},
                              {"verbose", no_argument, nullptr, 'v'},
                              {"output", required_argument, nullptr, 'o'},
                              {nullptr, no_argument, nullptr, 0}};

  while (true) {
    const auto opt = getopt_long(argc, argv, short_opts, long_opts, nullptr);

    if (opt == -1) {
      break;
    }
    switch (opt) {
      case 'v':
        verbose = true;
        break;
      case 'r':  // Runs given configuration
        config_file = optarg;
        break;
      case 'o':  // Sets the output file
        output_file = optarg;
        break;
      case 'c':  // Starts the configuration wizard
        create_config(optarg);
        return 0;
        break;
      case 'h':
        print_help();
        return 0;
        break;
      case '?':
      default:
        return 0;
        break;
    }
  }

  // Run miami if config given
  if (!config_file.empty()) {
    std::ifstream ifs;
    ifs.open(config_file);
    if (!ifs.good()) {
      std::cout << "Config file " << config_file << " not found!" << std::endl;
      return 0;
    }
    json jsonConfig;
    ifs >> jsonConfig;
    ifs.close();

    auto miami_config = new miami::Config();
    auto miami = new miami::Miami(miami_config);

    jsonConfig["config"]["verbose"] = verbose;
    miami->fromJson(jsonConfig);

    if (!miami_config->checkFiles().empty()) {
      return 0;
    }

    miami->run();

    if (!output_file.empty()) {
      std::ofstream ofs;
      ofs.open(output_file);
      ofs << std::setw(4) << miami->toJson();
      ofs.close();
    } else {
      // Overwrite config file
      std::ofstream ofs;
      ofs.open(config_file, std::ios::trunc);
      ofs << std::setw(4) << miami->toJson();
      ofs.close();
    }

    delete miami;
    delete miami_config;
  } else {
    std::cout << "Please specify configuration file with -r." << std::endl;
    std::cout << "See miami_suite --help for details!" << std::endl;
  }

  return 0;
}
