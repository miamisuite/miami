/** @file config.cpp
    @brief MIAMI configuration object
    @copyright Copyright (C) 2019 Christian-Alexander Dudek

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "config.hpp"

#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>

#include "tools.hpp"

// External headers
#include "nlohmann/json.hpp"
using json = nlohmann::json;

namespace miami {

/**
 * @brief      Constructs the configuration object.
 */
Config::Config() {
  this->labid_min_labeling = 0.05;
  this->labid_max_labeling = 0.5;
  this->labid_min_midsolv_r2 = 0.95;
  this->labid_dev_abs_sum = 0.2;
  this->identification_cutoff = 0.7;
  this->mid_identification_cutoff = 0.6;
  this->miami_error_per_isotopomer = 0.01;
  this->miami_distance_cutoff = 0.15;
  this->miami_variability_cutoff = 0.05;
  this->miami_gap_penalty = 0.2;

  this->miami_quant_cutoff = 0;
  this->miami_nodes_over_edges = true;
  this->miami_single_nodes = false;
  this->miami_show_arrows = false;
  this->miami_show_targets = false;
  this->miami_show_references = false;
  this->miami_remove_edges = false;
  this->miami_distance_method = miami_tools::Distance::Canberra;
  this->miami_distance_norm = miami_tools::Normalization::Sum;

  this->labid_required_replicates = 1.0;
  this->library_match_cutoff = 0.6;
  this->reference_files = {};
  this->reference_depth = 0;
  this->reference_full = false;
  this->reference_shortest = true;
  this->reference_remove = {};
  this->libraries = {};
  this->library_nist = "";
  this->nist_overwrite = false;
  this->verbose = false;
  this->tracer = std::string("C");
};

/**
 * @brief      Destroys the configuration object.
 */
Config::~Config() = default;

size_t Config::hash() { return std::hash<json>()(this->toJson()); }

/**
 * @brief      Adds a library.
 *
 * @param[in]  library_path  The library path
 */
void Config::addLibrary(const std::string& library_path) {
  this->libraries.emplace_back(library_path);
}

/**
 * @brief      Removes a library.
 *
 * @param[in]  library_path  The library path
 */
void Config::removeLibrary(const std::string& library_path) {
  auto itr =
      std::find(this->libraries.begin(), this->libraries.end(), library_path);
  if (itr != this->libraries.end()) {
    this->libraries.erase(itr);
  }
}

void Config::addFile(const std::string& name) { this->files.push_back(name); }

void Config::addFiles(const std::vector<std::string>& names) {
  this->files.insert(this->files.end(), names.begin(), names.end());
}

const std::vector<std::string> Config::getFiles() { return files; }

/**
 * @brief      Loads configuration from json file.
 *
 * @param[in]  config_filename  The configuration filename in json format
 */
void Config::loadJson(const std::string& config_filename) {
  std::ifstream input_stream(config_filename);
  json jsonConfig;
  input_stream >> jsonConfig;
  this->fromJson(jsonConfig["config"]);
}

/**
 * @brief      Saves the configuration in json format.
 *
 * @param[in]  config_filename  The configuration filename
 */
void Config::saveJson(const std::string& config_filename) {
  json json_config = this->toJson();
  std::ofstream output_stream(config_filename);
  output_stream << std::setw(2) << json_config << std::endl;
}

/**
 * @brief      Saves the configuration in json format.
 *
 * @returns  Vector of filenames which were not found.
 */
std::vector<std::string> Config::checkFiles() {
  std::vector<std::string> not_found = {};
  std::ifstream f;

  for (const auto& file_name : this->libraries) {
    f = std::ifstream(file_name.c_str());
    if (!f.good()) {
      if (this->verbose) {
        std::cout << "Library " << file_name << " not found!" << std::endl;
      }
      not_found.push_back(file_name);
    }
  }

  f = std::ifstream(this->library_nist);
  if (!this->library_nist.empty() && !f.good()) {
    if (this->verbose) {
      std::cout << "NIST library " << this->library_nist << " not found!"
                << std::endl;
    }
    not_found.push_back(this->library_nist);
  }

  for (const auto& file_name : this->getFiles()) {
    f = std::ifstream(file_name.c_str());
    if (!f.good()) {
      if (this->verbose) {
        std::cout << file_name << " not found!" << std::endl;
      }
      not_found.push_back(file_name);
    }
  }

  return not_found;
}

/**
 * @brief      Loads configuration from a json object
 *
 * @param[in]  jsonImport  The json configuration object
 */
void Config::fromJson(json jsonImport) {
  if (jsonImport.find("labid_min_labeling") != jsonImport.end()) {
    this->labid_min_labeling = jsonImport["labid_min_labeling"];
  }
  if (jsonImport.find("labid_max_labeling") != jsonImport.end()) {
    this->labid_max_labeling = jsonImport["labid_max_labeling"];
  }
  if (jsonImport.find("labid_min_midsolv_r2") != jsonImport.end()) {
    this->labid_min_midsolv_r2 = jsonImport["labid_min_midsolv_r2"];
  }
  if (jsonImport.find("labid_dev_abs_sum") != jsonImport.end()) {
    this->labid_dev_abs_sum = jsonImport["labid_dev_abs_sum"];
  }
  if (jsonImport.find("identification_cutoff") != jsonImport.end()) {
    this->identification_cutoff = jsonImport["identification_cutoff"];
  }
  if (jsonImport.find("mid_identification_cutoff") != jsonImport.end()) {
    this->mid_identification_cutoff = jsonImport["mid_identification_cutoff"];
  }
  if (jsonImport.find("miami_error_per_isotopomer") != jsonImport.end()) {
    this->miami_error_per_isotopomer = jsonImport["miami_error_per_isotopomer"];
  }
  if (jsonImport.find("miami_distance_cutoff") != jsonImport.end()) {
    this->miami_distance_cutoff = jsonImport["miami_distance_cutoff"];
  }
  if (jsonImport.find("miami_variability_cutoff") != jsonImport.end()) {
    this->miami_variability_cutoff = jsonImport["miami_variability_cutoff"];
  }


  if (jsonImport.find("miami_quant_cutoff") != jsonImport.end()) {
    this->miami_quant_cutoff = jsonImport["miami_quant_cutoff"];
  }
  if (!jsonImport["miami_nodes_over_edges"].is_null()) {
    this->miami_nodes_over_edges = jsonImport["miami_nodes_over_edges"].get<bool>();
  }
  if (!jsonImport["miami_single_nodes"].is_null()) {
    this->miami_single_nodes = jsonImport["miami_single_nodes"].get<bool>();
  }
  if (!jsonImport["miami_show_arrows"].is_null()) {
    this->miami_show_arrows = jsonImport["miami_show_arrows"].get<bool>();
  }
  if (!jsonImport["miami_show_targets"].is_null()) {
    this->miami_show_targets = jsonImport["miami_show_targets"].get<bool>();
  }
  if (!jsonImport["miami_show_references"].is_null()) {
    this->miami_show_references = jsonImport["miami_show_references"].get<bool>();
  }
  if (!jsonImport["miami_remove_edges"].is_null()) {
    this->miami_remove_edges = jsonImport["miami_remove_edges"].get<bool>();
  }

  if (jsonImport.find("miami_distance_method") != jsonImport.end()) {
    this->miami_distance_method =
        jsonImport["miami_distance_method"].get<miami_tools::Distance>();
  }
  if (jsonImport.find("miami_distance_norm") != jsonImport.end()) {
    this->miami_distance_norm =
        jsonImport["miami_distance_norm"].get<miami_tools::Normalization>();
  }
  if (jsonImport.find("labid_required_replicates") != jsonImport.end()) {
    this->labid_required_replicates = jsonImport["labid_required_replicates"];
  }
  if (jsonImport.find("library_match_cutoff") != jsonImport.end()) {
    this->library_match_cutoff = jsonImport["library_match_cutoff"];
  }
  if (jsonImport.find("tracer") != jsonImport.end()) {
    this->tracer = jsonImport["tracer"];
  }
  if (jsonImport.find("libraries") != jsonImport.end()) {
    this->libraries = jsonImport["libraries"].get<std::vector<std::string>>();
  }
  if (jsonImport.find("reference_files") != jsonImport.end()) {
    this->reference_files =
        jsonImport["reference_files"].get<std::vector<std::string>>();
  }
  if (jsonImport.find("reference_depth") != jsonImport.end()) {
    this->reference_depth = jsonImport["reference_depth"];
  }
  if (jsonImport.find("reference_full") != jsonImport.end()) {
    this->reference_full = jsonImport["reference_full"];
  }
  if (jsonImport.find("reference_shortest") != jsonImport.end()) {
    this->reference_shortest = jsonImport["reference_shortest"];
  }
  if (jsonImport.find("reference_remove") != jsonImport.end()) {
    this->reference_remove =
        jsonImport["reference_remove"].get<std::vector<std::string>>();
  }

  if (!jsonImport["library_nist"].is_null()) {
    this->library_nist = jsonImport["library_nist"];
    this->nist_overwrite = jsonImport["nist_overwrite"];
  }

  if (!jsonImport["verbose"].is_null()) {
    this->verbose = jsonImport["verbose"].get<bool>();
  }
}

/**
 * @brief      Returns a json representation of the configuration.
 *
 * @return     Json representation of the configuration.
 */
json Config::toJson() {
  json jsonExport;
  jsonExport["labid_min_labeling"] = this->labid_min_labeling;
  jsonExport["labid_max_labeling"] = this->labid_max_labeling;
  jsonExport["labid_min_midsolv_r2"] = this->labid_min_midsolv_r2;
  jsonExport["labid_dev_abs_sum"] = this->labid_dev_abs_sum;
  jsonExport["identification_cutoff"] = this->identification_cutoff;
  jsonExport["mid_identification_cutoff"] = this->mid_identification_cutoff;
  jsonExport["miami_error_per_isotopomer"] = this->miami_error_per_isotopomer;
  jsonExport["miami_distance_cutoff"] = this->miami_distance_cutoff;
  jsonExport["miami_variability_cutoff"] = this->miami_variability_cutoff;
  jsonExport["miami_quant_cutoff"] = this->miami_quant_cutoff;
  jsonExport["miami_nodes_over_edges"] = this->miami_nodes_over_edges;
  jsonExport["miami_single_nodes"] = this->miami_single_nodes;
  jsonExport["miami_show_arrows"] = this->miami_show_arrows;
  jsonExport["miami_show_targets"] = this->miami_show_targets;
  jsonExport["miami_show_references"] = this->miami_show_references;
  jsonExport["miami_remove_edges"] = this->miami_remove_edges;
  jsonExport["miami_distance_method"] = this->miami_distance_method;
  jsonExport["miami_distance_norm"] = this->miami_distance_norm;
  jsonExport["labid_required_replicates"] = this->labid_required_replicates;
  jsonExport["library_match_cutoff"] = this->library_match_cutoff;
  jsonExport["library_nist"] = this->library_nist;
  jsonExport["nist_overwrite"] = this->nist_overwrite;
  jsonExport["libraries"] = this->libraries;
  jsonExport["reference_files"] = this->reference_files;
  jsonExport["reference_depth"] = this->reference_depth;
  jsonExport["reference_full"] = this->reference_full;
  jsonExport["reference_shortest"] = this->reference_shortest;
  jsonExport["reference_remove"] = this->reference_remove;
  jsonExport["tracer"] = this->tracer;
  // NOTE(cdu): verbosity settings are currently not exported
  // jsonExport["verbose"] = this->verbose;
  return jsonExport;
}

}  // namespace miami
