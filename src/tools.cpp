/** @file tools.cpp
    @brief MIAMI tools
    @copyright Copyright (C) 2019 Christian-Alexander Dudek

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "tools.hpp"

#include <algorithm>
#define _USE_MATH_DEFINES
#include <cmath>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <map>
#include <regex>
#include <sstream>

static double DEFAULT_GAP_PENALTY = 0.2;

/**
 * @brief      The namespace for miami tools.
 */
namespace miami_tools {

/**
 * @brief      Calculates a two tailed t-test and returns the p-value.
 *
 * @param[in]  meanA  The first mean value
 * @param[in]  meanB  The second mean value
 * @param[in]  stdA   The first standard deviation
 * @param[in]  stdB   The second standard deviation
 * @param[in]  nA     The first population size
 * @param[in]  nB     The second population size
 *
 * @return     The p-value of the t-test
 */
double ttest(float meanA, float meanB, float stdA, float stdB, int nA, int nB) {
  double x = (stdA * stdA) / nA + (stdB * stdB) / nB;
  double t = (meanA - meanB) / sqrt(x);
  return pvalue(t, nA + nB - 2);
}

/**
 * @brief      Calculates the p-value of the given t-statistic and the degrees
 * of freedom.
 *
 * @param[in]  tstat  The t-statistic
 * @param[in]  dof    The degrees of freedom
 *
 * @return     The p-value
 */
double pvalue(double tstat, int dof) {
  double t = fabs(tstat);
  double w = t / sqrt(dof);
  double th = atan(w);
  if (dof == 1) {
    return 1 - th / M_PI_2;
  }
  double sth = sin(th);
  double cth = cos(th);
  double pvalue;
  if ((dof % 2) == 1) {
    pvalue =
        1 - (th + sth * cth * distribution(cth * cth, 2, dof - 3, -1)) / M_PI_2;
  } else {
    pvalue = 1 - sth * distribution(cth * cth, 1, dof - 3, -1);
  }
  return pvalue;
}

/**
 * @brief      Calculates some kind of cumulative distribution function
 *
 * @param[in]  q     Square
 * @param[in]  i     Start value
 * @param[in]  j     End value
 * @param[in]  b     Correction
 *
 * @return     Distribution value
 */
double distribution(double q, int i, int j, int b) {
  double zz = 1.0;
  double z = zz;

  while (i <= j) {
    zz = zz * q * i / (i - b);
    z += zz;
    i += 2;
  }
  return z;
}

/**
 * @brief      Extracts files by experiment name.
 *
 * @param[in]  experiment_name  The experiment name
 * @param      file_list        The file list
 *
 * @return     List with files matching the experiment name.
 */
std::vector<std::string> extractFiles(const std::string& experiment_name,
                                      std::vector<std::string>* file_list) {
  std::vector<std::string> experiment_files;
  auto re_group = std::regex(experiment_name);

  for (const auto& file : *file_list) {
    if (std::regex_search(file, re_group)) {
      experiment_files.emplace_back(file);
    }
  }

  file_list->erase(std::remove_if(file_list->begin(), file_list->end(),
                                  [re_group](std::string s) {
                                    return std::regex_search(s, re_group);
                                  }),
                   file_list->end());
  return experiment_files;
}

/**
 * @brief      Performs Needleman-Wunsch alignment.
 *
 * @param[in]  v1    The first vector of mean/err pairs
 * @param[in]  v2    The second vector of mean/err pairs
 *
 * @return     A pair of aligned vectors.
 */
std::pair<std::vector<double>, std::vector<double>> needlemanWunsch(
    std::vector<std::pair<double, double>> v1,
    std::vector<std::pair<double, double>> v2) {
  std::vector<double> w1;
  w1.reserve(v1.size());
  std::vector<double> w2;
  w2.reserve(v2.size());
  // Get mean MID values
  for (auto p : v1) {
    w1.push_back(p.first);
  }
  for (auto p : v2) {
    w2.push_back(p.first);
  }
  return needlemanWunsch(w1, w2, DEFAULT_GAP_PENALTY);
}

/**
 * @brief      Performs Needleman-Wunsch alignment.
 *
 * @param[in]  v1           The first vector of mean/err pairs.
 * @param[in]  v2           The second vector of mean/err pairs.
 * @param[in]  gap_penalty  The penalty for gap introduction
 *
 * @return     A pair of aligned vectors.
 */
std::pair<std::vector<double>, std::vector<double>> needlemanWunsch(
    std::vector<std::pair<double, double>> v1,
    std::vector<std::pair<double, double>> v2, double gap_penalty) {
  std::vector<double> w1;
  w1.reserve(v1.size());
  std::vector<double> w2;
  w2.reserve(v2.size());
  // Get mean MID values
  for (auto p : v1) {
    w1.push_back(p.first);
  }
  for (auto p : v2) {
    w2.push_back(p.first);
  }
  return needlemanWunsch(w1, w2, gap_penalty);
}

/**
 * @brief      Performs Needleman-Wunsch alignment.
 *
 * @param[in]  v1    The first vector of mean values
 * @param[in]  v2    The second vector of mean values
 *
 * @return     A pair of aligned vectors.
 */
std::pair<std::vector<double>, std::vector<double>> needlemanWunsch(
    std::vector<double> v1, std::vector<double> v2) {
  return needlemanWunsch(std::move(v1), std::move(v2), DEFAULT_GAP_PENALTY);
}

/**
 * @brief      Performs Needleman-Wunsch alignment.
 *
 * @param[in]  v1           The first vector of mean values
 * @param[in]  v2           The second vector of mean values
 * @param[in]  gap_penalty  The gap penalty
 *
 * @return     A pair of aligned vectors.
 */
std::pair<std::vector<double>, std::vector<double>> needlemanWunsch(
    std::vector<double> v1, std::vector<double> v2, double gap_penalty) {
  std::vector<std::vector<double>> score_matrix;
  std::vector<std::vector<char>> trace_matrix;

  double cur_gap_penalty = gap_penalty;

  // Initialize matrices
  score_matrix.resize(v1.size() + 1);
  trace_matrix.resize(v1.size() + 1);

  for (int i = 0; i <= v1.size(); ++i) {
    score_matrix[i].resize(v2.size() + 1);
    score_matrix[i][0] = gap_penalty * i;

    trace_matrix[i].resize(v2.size() + 1);
    trace_matrix[i][0] = '|';
  }

  for (int i = 0; i <= v2.size(); ++i) {
    score_matrix[0][i] = gap_penalty * i;
    trace_matrix[0][i] = '-';
  }
  trace_matrix[0][0] = '*';

  // Fill the matrices
  for (int row = 1; row <= v1.size(); ++row) {
    for (int col = 1; col <= v2.size(); ++col) {
      // Calculate the scores
      double min_score;
      char min_path;

      double sright;
      if (row == v1.size()) {
        // Tailing gaps are less expensive!
        sright = score_matrix[row][col - 1];
      } else {
        sright = score_matrix[row][col - 1] + cur_gap_penalty;
      }
      double sdown;
      if (col == v2.size()) {
        // Tailing gaps are less expensive!
        sdown = score_matrix[row - 1][col];
      } else {
        sdown = score_matrix[row - 1][col] + cur_gap_penalty;
      }
      // Save minimum score for gaps
      if (sdown < sright) {
        min_score = sdown;
        min_path = '|';
      } else {
        min_score = sright;
        min_path = '-';
      }

      double sdiag =
          score_matrix[row - 1][col - 1] + std::fabs(v1[row - 1] - v2[col - 1]);
      if (sdiag < min_score) {
        min_score = sdiag;
        min_path = '\\';
      }
      // Save best score/path
      score_matrix[row][col] = min_score;
      trace_matrix[row][col] = min_path;
    }
  }

  // Alignment traceback
  std::vector<double> a1, a2;
  int row = v1.size();
  int col = v2.size();
  while (row > 0 || col > 0) {
    switch (trace_matrix[row][col]) {
      case '\\':
        a1.push_back(v1[--row]);
        a2.push_back(v2[--col]);
        break;
      case '|':
        a1.push_back(v1[--row]);
        a2.push_back(0.0);
        break;
      case '-':
        a1.push_back(0.0);
        a2.push_back(v2[--col]);
        break;
      default:
        row = 0;
        col = 0;
        break;
    }
  }
  std::reverse(a1.begin(), a1.end());
  std::reverse(a2.begin(), a2.end());

  // debug(stringifyMatrix(score_matrix));
  // debug(stringifyMatrix(trace_matrix));

  return std::pair<std::vector<double>, std::vector<double>>(a1, a2);
}

/**
 * @brief      Returns a string representation of a matrix.
 *
 * @param[in]  matrix  The matrix of values
 *
 * @return     The matrix in string format.
 */
std::string stringifyMatrix(const std::vector<std::vector<double>>& matrix) {
  std::stringstream ss;
  ss << std::fixed << std::setprecision(2) << std::setw(3);
  for (auto row : matrix) {
    for (auto cell : row) {
      ss << cell << "  ";
    }
    ss << std::endl;
  }
  return ss.str();
}

/**
 * @brief      Returns a string representation of a matrix.
 *
 * @param[in]  matrix  The matrix with characters
 *
 * @return     The matrix in string format.
 */
std::string stringifyMatrix(const std::vector<std::vector<char>>& matrix) {
  std::stringstream ss;
  for (auto row : matrix) {
    for (auto cell : row) {
      ss << cell << "";
    }
    ss << std::endl;
  }
  return ss.str();
}

/**
 * @brief      Returns a string representation of a matrix.
 *
 * @param[in]  matrix  The matrix of node/value pairs
 *
 * @return     The matrix in string format.
 */
std::string stringifyMatrix(std::map<int, std::map<int, double>> matrix) {
  std::stringstream ss;
  ss << std::fixed << std::setprecision(2);
  ss << std::setw(5) << std::right << " Node"
     << " | ";
  for (auto row : matrix) {
    ss << std::setw(4) << std::right << row.first << "  ";
  }
  ss << std::endl;
  ss << std::setw(5) << std::setfill('-') << "-"
     << "-+-";
  for (const auto& row : matrix) {
    ss << std::setw(4) << "-"
       << "--";
  }
  ss << std::endl;
  ss << std::setfill(' ');
  for (auto row : matrix) {
    bool first = true;
    for (auto col : matrix) {
      if (first) {
        ss << std::setw(5) << std::right << row.first << " | ";
        first = false;
      }
      ss << matrix[std::min(row.first, col.first)]
                  [std::max(row.first, col.first)]
         << "  ";
    }
    ss << std::endl;
  }
  return ss.str();
}

/**
 * @brief      Returns a string representation of an alignement.
 *
 * @param[in]  alignment  The alignment as pair of vectors
 *
 * @return     The alignment string.
 */
std::string stringifyAlignment(
    const std::pair<std::vector<double>, std::vector<double>>& alignment) {
  std::stringstream ss;
  ss << std::fixed << std::setprecision(3) << std::setw(5);
  ss << "Vector 1: ";
  for (auto d : alignment.first) {
    ss << d << "  ";
  }
  ss << std::endl;
  ss << "Vector 2: ";
  for (auto d : alignment.second) {
    ss << d << "  ";
  }
  return ss.str();
}

/**
 * @brief      Calculates the distance between two vectors.
 *
 * @param[in]  v1                    The first vector of mean/error pairs
 * @param[in]  v2                    The second vector of mean/error pairs
 * @param[in]  distance_method       The distance method
 * @param[in]  normalization_method  The normalization method
 *
 * @return     The distance.
 */
double distance(const std::vector<std::pair<double, double>>& v1,
                const std::vector<std::pair<double, double>>& v2,
                Distance distance_method, Normalization normalization_method) {
  std::vector<double> w1;
  w1.reserve(v1.size());
  std::vector<double> w2;
  w2.reserve(v1.size());
  // Get mean MID values
  for (auto p : v1) {
    w1.push_back(p.first);
  }
  for (auto p : v2) {
    w2.push_back(p.first);
  }
  return distance(w1, w2, distance_method, normalization_method);
}

/**
 * @brief      Calculates the distance between two vectors.
 *
 * @param[in]  v1                    The first vector of values
 * @param[in]  v2                    The second vector of values
 * @param[in]  distance_method       The distance method
 * @param[in]  normalization_method  The normalization method
 *
 * @return     The distance.
 */
double distance(const std::vector<double>& v1, const std::vector<double>& v2,
                Distance distance_method, Normalization normalization_method) {
  // Align the vectors
  std::pair<std::vector<double>, std::vector<double>> alignment =
      needlemanWunsch(v1, v2);
  std::vector<double> a1 = alignment.first;
  std::vector<double> a2 = alignment.second;

  double distance;

  // Calculate the distance based on given method
  switch (distance_method) {
    case Distance::Euclidean:
      distance = euclideanDistance(a1, a2);
      break;
    case Distance::Manhattan:
      distance = manhattanDistance(a1, a2);
      break;
    default:  // Default distance method: Canberra Distance
      distance = canberraDistance(a1, a2);
      break;
  }

  // Normalize by given method
  switch (normalization_method) {
    case Normalization::Min:
      distance /= std::min(v1.size(), v2.size());
      break;
    case Normalization::Max:
      distance /= std::max(v1.size(), v2.size());
      break;
    case Normalization::Sum:
      distance /= (v1.size() + v2.size());
      break;
    case Normalization::Prod:
      distance /= (v1.size() * v2.size());
      break;
    default:  // No normalization Normalization::None
      break;
  }

  return distance;
}

/**
 * @brief      Returns the euclidean distance between two aligned vectors.
 *
 * @param[in]  v1    The first vector
 * @param[in]  v2    The second vector
 *
 * @return     The euclidean distance.
 */
double euclideanDistance(const std::vector<double>& v1,
                         const std::vector<double>& v2) {
  double d = 0;
  for (int i = 0; i < v1.size(); ++i) {
    d += (v1[i] - v2[i]) * (v1[i] - v2[i]);
  }
  return sqrt(d);
}

/**
 * @brief      Returns the canberra distance between two aligned vectors.
 *
 * @param[in]  v1    The first vector
 * @param[in]  v2    The second vector
 *
 * @return     The canberra distance.
 */
double canberraDistance(const std::vector<double>& v1,
                        const std::vector<double>& v2) {
  double d = 0;
  for (int i = 0; i < v1.size(); ++i) {
    d += fabs(v1[i] - v2[i]) / (fabs(v1[i]) + fabs(v2[i]));
  }
  return d;
}

/**
 * @brief      Returns the manhattan distance between two aligned vectors.
 *
 * @param[in]  v1    The first vector
 * @param[in]  v2    The second vector
 *
 * @return     The manhattan distance.
 */
double manhattanDistance(const std::vector<double>& v1,
                         const std::vector<double>& v2) {
  double d = 0;
  for (int i = 0; i < v1.size(); ++i) {
    d += fabs(v1[i] - v2[i]);
  }
  return d;
}

/**
 * @brief      Calculates the fractional contribution of a vector.
 *
 * @param[in]  vec   The vector, usually mean MIDs with std
 *
 * @return     The fractional contribution.
 */
double fractionalContribution(
    const std::vector<std::pair<double, double>>& vec) {
  double fc = 0;
  for (int i = 0; i < vec.size(); i++) {
    fc += vec[i].first * i;
  }
  fc /= vec.size() - 1;
  return fc;
}

std::vector<std::string> getNeighbours(
    const std::string& node_id,
    const std::vector<std::vector<std::string>>& edges) {
  std::vector<std::string> neighbours;
  for (auto v : edges) {
    std::string key = v[0];
    std::string val = v[1];
    if (key == node_id && std::find(neighbours.begin(), neighbours.end(),
                                    val) == neighbours.end()) {
      neighbours.emplace_back(val);
    } else if (val == node_id && std::find(neighbours.begin(), neighbours.end(),
                                           key) == neighbours.end()) {
      neighbours.emplace_back(key);
    }
  }
  return neighbours;
}

/**
 * @brief      Finds all paths between two nodes in a given network.
 *
 * @param[in]  from_id    The kegg id from
 * @param[in]  to_id      The kegg id to
 * @param[in]  edges      The edges as vector of pairs
 * @param[in]  shortest   If true, returns only the shortest path
 * @param[in]  depth      The maximum length of a path, if 0 no limit will be
 *                        applied
 *
 * @return     Vector of all paths as vectors with kegg ids.
 */
std::vector<std::vector<std::string>> findPath(
    const std::string& from_id, const std::string& to_id,
    const std::vector<std::vector<std::string>>& edges, bool shortest,
    int depth) {
  std::vector<std::vector<std::string>> paths;
  std::vector<std::string> path;
  std::string node = from_id;
  std::vector<std::pair<std::string, std::vector<std::string>>> queue;

  queue.emplace_back(
      std::pair<std::string, std::vector<std::string>>(node, path));

  while (!queue.empty()) {
    node = queue.front().first;
    path = queue.front().second;
    queue.erase(queue.begin());

    // If the path length already exceeds the depth,
    // do not go further and skip this queue item
    if (depth > 0 && path.size() > depth) {
      continue;
    }

    // Get all neighbours and check if target node was found
    for (auto n : getNeighbours(node, edges)) {
      if (std::find(path.begin(), path.end(), n) == path.end()) {
        std::vector<std::string> tmp = path;
        tmp.emplace_back(n);

        if (n == to_id) {
          paths.emplace_back(tmp);
          if (shortest) {
            return paths;
          }
        } else {
          queue.emplace_back(std::pair(n, tmp));
        }
      }
    }
  }
  if (paths.empty()) {
    paths = {{}};
  }
  return paths;
}
}  // namespace miami_tools
