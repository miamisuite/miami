/** @file tools.hpp
    @brief MIAMI tools
    @copyright Copyright (C) 2019 Christian-Alexander Dudek

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef TOOLS_HPP
#define TOOLS_HPP

#include <iostream>
#include <map>
#include <vector>

// External headers
#include <nlohmann/json.hpp>
using json = nlohmann::json;

namespace miami_tools {
/**
 * @brief      The distance measurements.
 */
enum Distance {
  Euclidean, /**< Euclidean distance: \f$\sqrt{\sum_{i=0}^{n} (v1_i -
                v2_i)^2}\f$*/
  Canberra,  /**< Canberra distance: \f$\sum_{i=0}^{n} \frac{|v1_i -
                v2_i|}{|v1_i| + |v2_i|}\f$ */
  Manhattan  /**< Manhattan distance:  \f$\sum_{i=0}^{n} |v1_i -
                v2_i|\f$ */
};
// map enums to JSON as strings
NLOHMANN_JSON_SERIALIZE_ENUM(Distance, {
                                           {Euclidean, "Euclidean"},
                                           {Canberra, "Canberra"},
                                           {Manhattan, "Manhattan"},
                                       });

/**
 * @brief      The normalization method.
 */
enum Normalization {
  None, /**< No normalization */
  Sum,  /**< Normalization by the sum of values */
  Prod, /**< Normalization by the product of values */
  Max,  /**< Normalization by the max value */
  Min   /**< Normalization by the min value */
};
// map Normalization values to JSON as strings
NLOHMANN_JSON_SERIALIZE_ENUM(Normalization, {
                                                {None, "None"},
                                                {Sum, "Sum"},
                                                {Prod, "Prod"},
                                                {Max, "Max"},
                                                {Min, "Min"},
                                            });

double ttest(float meanA, float meanB, float stdA, float stdB, int nA, int nB);
double pvalue(double tstat, int dof);
double distribution(double q, int i, int j, int b);

std::vector<std::string> extractFiles(const std::string& experiment_name,
                                      std::vector<std::string>* file_list);
std::pair<std::vector<double>, std::vector<double>> needlemanWunsch(
    std::vector<std::pair<double, double>> v1,
    std::vector<std::pair<double, double>> v2);
std::pair<std::vector<double>, std::vector<double>> needlemanWunsch(
    std::vector<std::pair<double, double>> v1,
    std::vector<std::pair<double, double>> v2, double gapPenalty);
std::pair<std::vector<double>, std::vector<double>> needlemanWunsch(
    std::vector<double> v1, std::vector<double> v2);
std::pair<std::vector<double>, std::vector<double>> needlemanWunsch(
    std::vector<double> v1, std::vector<double> v2, double gapPenalty);

std::string stringifyMatrix(const std::vector<std::vector<double>>& matrix);
std::string stringifyMatrix(const std::vector<std::vector<char>>& matrix);
std::string stringifyMatrix(std::map<int, std::map<int, double>> matrix);
std::string stringifyAlignment(
    const std::pair<std::vector<double>, std::vector<double>>& alignment);

double distance(const std::vector<double>& v1, const std::vector<double>& v2,
                Distance distance_method = Distance::Canberra,
                Normalization normalization_method = Normalization::Sum);
double distance(const std::vector<std::pair<double, double>>& v1,
                const std::vector<std::pair<double, double>>& v2,
                Distance distance_method = Distance::Canberra,
                Normalization normalization_method = Normalization::Sum);
double euclideanDistance(const std::vector<double>& v1,
                         const std::vector<double>& v2);
double canberraDistance(const std::vector<double>& v1,
                        const std::vector<double>& v2);
double manhattanDistance(const std::vector<double>& v1,
                         const std::vector<double>& v2);
double fractionalContribution(
    const std::vector<std::pair<double, double>>& vec);
std::vector<std::vector<std::string>> findPath(
    const std::string& from_id, const std::string& to_id,
    const std::vector<std::vector<std::string>>& edges, bool shortest = true,
    int depth = 0);
std::vector<std::string> getNeighbours(
    const std::string& node_id,
    const std::vector<std::vector<std::string>>& edges);
}  // namespace miami_tools
#endif  // TOOLS_HPP
