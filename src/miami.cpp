/** @file miami.cpp
    @brief MIAMI object
    @copyright Copyright (C) 2019 Christian-Alexander Dudek

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "miami.hpp"
#include "config.hpp"
#include "experiment.hpp"
#include "node.hpp"
#include "tools.hpp"

#include <gzstream.h>
#include <time.h>
#include <unistd.h>
#include <chrono>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

// Labid headers
#include "labeledcompound.h"
#include "labelidentificator.h"

// GCMS headers
#include "compound.h"
#include "defaultpeakdetector.h"
#include "gcmsdiskscan.h"
#include "librarycompound.h"
#include "midcompoundseries.h"
#include "naturalmidcalculator.h"
#include "replicatemiddata.h"
#include "replicateresultseries.h"

// External headers
#include <nlohmann/json.hpp>

/**
 * @brief      The miami namespace.
 */
namespace miami {

/**
 * @brief      Constructs the MIAMI object.
 *
 * @param      config_  The configuration object
 */
Miami::Miami(Config* config_) : config(config_), lid(nullptr){};

/**
 * @brief      Destroys the object.
 */
Miami::~Miami() {
  // Libraries for identification are not needed anymore
  for (auto idlib : identification_libs) {
    delete idlib;
  }
  for (auto exp : this->experiments) {
    delete exp;
  }
}

std::vector<miami::Experiment*> Miami::getExperiments() {
  return this->experiments;
}

/**
 * @brief      Adds an experiment.
 *
 * @param[in]  experiment_name  The exponent name
 * @param[in]  labeled_files    The labeled files
 * @param[in]  unlabeled_files  The unlabeled files
 *
 * @return     True if successfull added, false otherwise
 */
bool Miami::addExperiment(const std::string& experiment_name,
                          const std::vector<std::string>& labeled_files,
                          const std::vector<std::string>& unlabeled_files) {
  bool exp_added = false;

  bool exp_loaded = false;
  for (auto exp : this->experiments) {
    if (exp->getName() == experiment_name) {
      exp_loaded = true;
      break;
    }
  }

  if (!exp_loaded) {
    this->config->addFiles(labeled_files);
    this->config->addFiles(unlabeled_files);

    this->experiments.push_back(
        new miami::Experiment(experiment_name, &this->nodes, labeled_files,
                              unlabeled_files, this->config));
    exp_added = true;
  };
  return exp_added;
}

void Miami::removeExperiment(const std::string& name) {
  int i = 0;
  for (auto exp : this->experiments) {
    if (exp->getName() == name) {
      delete exp;
      this->experiments.erase(this->experiments.begin() + i);
      break;
    }
    i++;
  }
}

void Miami::removeExperiment(const int id) {
  delete this->experiments[id];
  this->experiments.erase(this->experiments.begin() + id);
}

void Miami::changeExperiment(const int id, const std::string& experiment_name,
                             const std::vector<std::string>& labeled_files,
                             const std::vector<std::string>& unlabeled_files) {

  auto exp = this->experiments[id];
  exp->setName(experiment_name);
  exp->setLabeledFiles(labeled_files);
  exp->setUnlabeledFiles(unlabeled_files);
}

/**
 * @brief      Loads a library.
 *
 * @param[in]  library_filename  The library filename
 *
 * @return     True if successfull added, false otherwise
 */
bool Miami::loadLibrary(const std::string& library_filename) {
  bool lib_added = false;
  // Add library to config if not present
  if (std::find(this->config->libraries.begin(), this->config->libraries.end(),
                library_filename) == this->config->libraries.end()) {
    this->config->addLibrary(library_filename);
    lib_added = true;
  }
  bool lib_loaded = false;
  for (auto lib : this->identification_libs) {
    if (lib->getFileName() == library_filename) {
      lib_loaded = true;
      break;
    }
  }
  if (!lib_loaded) {
    this->identification_libs.push_back(
        gcms::LibrarySearch<int, float>::fromDisk(library_filename.c_str()));
    lib_added = true;
  }
  return lib_added;
}

/**
 * @brief      Removes a library.
 *
 * @param[in]  library_filename  The library filename
 */
void Miami::removeLibrary(const std::string& library_filename) {
  int i = 0;
  for (auto lib : this->identification_libs) {
    if (lib->getFileName() == library_filename) {
      delete lib;
      this->identification_libs.erase(this->identification_libs.begin() + i);
      this->config->removeLibrary(library_filename);
      break;
    }
    i++;
  }
}

void Miami::printLibraries() {
  std::cout << "Loaded libraries:" << std::endl;
  for (auto lib : this->identification_libs) {
    std::cout << lib->getFileName() << std::endl;
  }
}

/**
 * @brief      Saves a library.
 *
 * @param[in]  library_filename  The library filename
 */
void Miami::saveLibrary(const std::string& library_filename) {
  if (export_msl) {
    this->internal_lib->exportMSL(
        (library_filename + std::string(".msl")).c_str());
  }
  if (export_lbr) {
    this->internal_lib->toDisk(
        (library_filename + std::string(".lbr")).c_str());
  }
}

/**
 * @brief      Runs the MIAMI processing pipeline.
 */
void Miami::run() {
  std::vector<gcms::LibraryHit<int, float>> lib_hits;
  if (!this->config->library_nist.empty()) {
    nist_lib = new gcms::SQLLibrary(this->config->library_nist.c_str());
  } else {
    nist_lib = nullptr;
  }

  this->done = false;

  for (auto node : this->nodes) {
    delete node.second;
  }

  this->current_id = 0;
  this->nodes.clear();
  this->reference_nodes.clear();
  this->ref_to_ref_edges.clear();
  this->node_to_node_edges.clear();
  this->node_to_ref_edges.clear();

  internal_lib = new gcms::LibrarySearch<int, float>();

  bool first_experiment = true;
  int node_id = -1;

  bool verbose = this->config->verbose;

  if (verbose) {
    std::cout << std::endl << "MIAMI v" << MIAMI_VERSION << std::endl;
    std::cout << "Detect labeling...";
  }

  for (auto experiment : this->experiments) {
    experiment->detectLabeledMetabolites();
    // Iterate over labeled compounds
    for (auto labeled_metabolite : experiment->getLabeledMetabolites()) {
      if (labeled_metabolite->getLabeledIons().empty()) {
        continue;  // skip if no proper label detected
      }

      auto labeled_reference =
          new gcms::LibraryCompound<int, float>(*labeled_metabolite);
      gcms::LibraryCompound<int, float>* metabolite_match = nullptr;

      // TODO(cdu): Use Exclude lib?

      // Set quantification ions
      std::set<float> ions;
      for (auto frag : labeled_reference->getFragments()) {
        for (int iIon = std::min(frag.first, frag.second);
             iIon <= std::max(frag.first, frag.second); iIon++) {
          ions.insert(float(iIon));
        }
      }

      labeled_reference->setQuantificationIons(ions);

      if (!first_experiment) {
        // Check if similar compound is already in library and use
        // current_id to identify across experiments.
        lib_hits = this->internal_lib->getLibraryHits(*labeled_reference);
        if (!lib_hits.empty() && lib_hits.at(0).getOverallScore() >=
                                     this->config->library_match_cutoff) {
          metabolite_match = lib_hits.at(0).getLibraryCompound();
          node_id = std::stoi(metabolite_match->getFeature("NODE_ID"));
          labeled_reference->addFeature(
              "NODE_ID", metabolite_match->getFeature("NODE_ID"));
          labeled_reference->setName(metabolite_match->getName());

          // Add reference metabolite to node
          this->nodes[node_id]->addSpectrum(labeled_reference);

          this->internal_lib->addCompoundSpectrum(*labeled_reference);
        }
      }

      // Metabolite not yet found
      if (metabolite_match == nullptr) {
        labeled_reference->addFeature("NODE_ID", std::to_string(current_id));
        this->nodes[current_id] = new miami::Node(current_id);
        this->nodes[current_id]->setFiles(experiment->numLabeledFiles(),
                                          experiment->numUnlabeledFiles());

        // Identify new metabolite
        double max_score = -1.0;
        std::string cur_name;
        std::string kegg_id;
        std::string source = "None";

        // Use the RI only if metabolite is calibrated
        gcms::GCMSSettings::LS_USE_RI =
            labeled_reference->getRetentionIndex() != -1.0;
        gcms::GCMSSettings::LS_RI_DIFF = 100;
        gcms::GCMSSettings::LS_PURE_FACTOR = 0.5;
        gcms::GCMSSettings::LS_IMPURE_FACTOR = 0.5;

        // gcms::GCMSSettings::LS_MASS_FILTER = s.cmp_id_mass_filter;
        for (auto lib : identification_libs) {
          lib_hits = lib->getLibraryHits(*labeled_reference);
          if (!lib_hits.empty() &&
              lib_hits.at(0).getOverallScore() >=
                  this->config->identification_cutoff &&
              lib_hits.at(0).getOverallScore() > max_score) {
            max_score = lib_hits.at(0).getOverallScore();
            cur_name = lib_hits.at(0).getLibraryCompound()->getName();
            source = lib->getFileName();

            // TODO(cdu): Add multiple feature names to config?
            // This is the kegg id from TUBS libraries
            kegg_id = lib_hits.at(0).getLibraryCompound()->getFeature(
                "PRECURSOR_KEGG_ID");

            // Alternatively check for MET_KEGG feature from golm
            if (kegg_id.empty()) {
              kegg_id =
                  lib_hits.at(0).getLibraryCompound()->getFeature("MET_KEGG");
            }
            // Only use first kegg id if multiple given
            std::stringstream ss(kegg_id);
            std::getline(ss, kegg_id, ',');
          }
        }

        // Perform nist search if nist is loaded and if either no hit found yet
        // or overwrite with NIST is explicitly allowed!
        if (nist_lib != nullptr &&
            (max_score == -1 || this->config->nist_overwrite)) {
          std::vector<gcms::LibraryHit<int, float>> nist_hit;

          nist_lib->deleteCachedCompounds();
          nist_hit = nist_lib->getLibraryHits(*labeled_reference, 1.0);
          if (!nist_hit.empty() &&
              nist_hit.at(0).getOverallScore() >=
                  this->config->identification_cutoff &&
              nist_hit.at(0).getOverallScore() > max_score) {
            max_score = nist_hit.at(0).getOverallScore();
            cur_name = nist_hit.at(0).getLibraryCompound()->getName();
            source = this->config->library_nist;
            // Nist does not provide a link to kegg!
            kegg_id = "";
          }
        }

        labeled_reference->addFeature("lib_source", source);

        if (max_score > -1.0) {
          labeled_reference->setName(cur_name);
          labeled_reference->addFeature("PRECURSOR_KEGG_ID", kegg_id);
        } else {
          std::stringstream ss;
          // ss << "RT " << labeled_reference->getRetentionTime() / 1000.0
          // / 60.0;
          ss << "RI " << labeled_reference->getRetentionIndex();
          labeled_reference->setName(ss.str());
          cur_name = ss.str();
        }

        this->nodes[current_id]->setName(cur_name);
        gcms::LibraryCompound<int, float>* tmp(labeled_reference);
        this->nodes[current_id]->setReference(tmp, max_score, source);

        current_id++;

        // Add metabolite to library
        this->internal_lib->addCompound(*labeled_reference);
      }
    }

    first_experiment = false;
  }

  if (verbose) {
    std::cout << " done!" << std::endl << "Targeted/non-targeted MIDs...";
  }

  // Create map of labeled files for every experiment
  // and read all compounds to one big Library
  std::map<std::string, std::list<std::string>> experiment_file_mapping;
  std::vector<std::string> all_labeled_files;
  gcms::LibrarySearch<int, float> compound_lib;

  // Create experiment mapping
  for (auto experiment : this->experiments) {
    for (const auto& f : experiment->getLabeledFiles()) {
      experiment_file_mapping[experiment->getName()].emplace_back(f);
      all_labeled_files.emplace_back(f);
    }
  }

  // MIDs
  // Do the targeted search with reference library correction
  gcms::ReplicateMIDData* mid_data = searchTargetedMids(
      all_labeled_files, experiment_file_mapping, this->internal_lib, true);

  const std::vector<std::string>& groups = mid_data->getGroupNames();

  for (auto mid_series : mid_data->getMIDCompoundSeries()) {
    int node_id = std::stoi(
        mid_series->getReferenceLibraryCompound().getFeature("NODE_ID"));

    std::map<std::string, std::map<int, std::vector<std::pair<double, double>>>>
        node_mids;

    // Iterate ions
    for (int iIon = 0; iIon < mid_series->getIonCount(); ++iIon) {
      int ion = mid_series->getFragmentIon(iIon);
      // for (int iGroup = 0; iGroup < groups.size(); ++iGroup) {
      for (const auto& group : groups) {
        // TODO(cdu): Do we really want to ignore zero values for Mean/StdErr
        // calculation?
        double mi =
            mid_data->getGroupMeanMIAbundance(group, *mid_series, iIon, true);
        double err = mid_data->getGroupStdErrorMIAbundance(group, *mid_series,
                                                           iIon, true);

        node_mids[group][ion].push_back(std::make_pair(mi, err));
      }
    }
    this->nodes[node_id]->setMids(node_mids);
  }

  // Quantification
  if (verbose) {
    std::cout << " done!" << std::endl
              << "Targeted/non-targeted quantification...";
  }
  // if(this->config->quantification) {
  gcms::ReplicateResultSeries<int, float>* quant_data = searchTargetedQuant(
      all_labeled_files, experiment_file_mapping, this->internal_lib);

  for (auto quant_series : quant_data->getAllCompoundSeries()) {
    int node_id =
        std::stoi(quant_series->getReferenceCompound()->getFeature("NODE_ID"));
    for (const auto& group : quant_data->getGroupNames()) {
      double mean = quant_data->getGroupMean(group, *quant_series, true);
      double err = quant_data->getGroupStdDev(group, *quant_series, true);
      this->nodes[node_id]->setQuant(group, mean, err);
    }
  }
  // }

  // We iterate over all nodes backwards, find the largest common ion and
  // delete the node if we don't get a common ion!
  if (verbose) {
    std::cout << " done!" << std::endl << "Check nodes... ";
  }
  for (int node_id = this->nodes.size() - 1; node_id >= 0; --node_id) {
    auto node = this->nodes[node_id];
    node->findLargestCommonIon(this->config->miami_error_per_isotopomer);
    if (!node->isValid()) {
      if (verbose) {
        std::cout << " " << node->getName();
      }
      delete node;                 // Delete Node object
      node = nullptr;              // Set NULL Pointer
      this->nodes.erase(node_id);  // Remove from nodes list
    } else {
      // Calculate variability between experiments
      node->calculateVariability();
    }
  }
  if (verbose) {
    std::cout << " are invalid!" << std::endl << "Calculate distances...";
  }

  // Calculate distances
  for (auto pExperiment : this->experiments) {
    pExperiment->calculateDistances();
  }

  if (verbose) {
    std::cout << " done!" << std::endl
              << "Map metabolites to reference pathway...";
  }
  // Include reference nodes and edges
  std::map<std::string, miami::Node*> nodes_with_ref;

  for (auto const& reference_file : this->config->reference_files) {
    json jsonRef;
    // Load data from file
    std::ifstream input_stream(reference_file);
    input_stream >> jsonRef;

    // Load reference nodes and edges
    std::vector<std::vector<std::string>> refEdges = jsonRef["edges"];
    std::map<std::string, std::string> refNodes = jsonRef["nodes"];

    // Iterate all nodes and check if metabolite is in reference
    for (auto const& [node_id, node] : this->nodes) {
      if (!node->getKegg().empty() &&
          refNodes.find(node->getKegg()) != refNodes.end()) {
        nodes_with_ref[node->getKegg()] = node;
      }
    }

    if (!this->config->reference_full) {
      if (verbose) {
        std::cout << " done!" << std::endl << "Search paths on reference...";
      }
      // Do path search on reference if no full path is used
      std::vector<std::pair<std::string, std::string>> done;

      // Find paths between all nodes in reference pathway
      for (const auto& [n_from, n1] : nodes_with_ref) {
        for (const auto& [n_to, n2] : nodes_with_ref) {
          // Skip if nodes are the same
          if (n_from == n_to) {
            continue;
          }
          // Skip is one node has no neighbours
          size_t x_from = miami_tools::getNeighbours(n_from, refEdges).size();
          size_t x_to = miami_tools::getNeighbours(n_to, refEdges).size();
          if (x_from == 0 || x_to == 0) {
            continue;
          }
          std::pair<std::string, std::string> cur = std::minmax(n_from, n_to);

          // Calculate path between nodes if pair has not been
          // processed yet
          if (std::find(done.begin(), done.end(), cur) == done.end()) {
            auto curPath = miami_tools::findPath(
                n_from, n_to, refEdges, this->config->reference_shortest,
                this->config->reference_depth);
            int paths;
            if (this->config->reference_shortest) {
              paths = 1;
            } else {
              paths = curPath.size();
            }
            std::vector<std::string> v = this->config->reference_remove;
            for (int i = 0; i < paths; i++) {
              for (const auto& k : curPath[i]) {
                // Check for nodes to remove, values can either be the name or
                // kegg id Save all nodes from the path
                if (std::find(v.begin(), v.end(), k) == v.end() &&
                    std::find(v.begin(), v.end(), refNodes[k]) == v.end() &&
                    k != n_to && k != n_from &&
                    nodes_with_ref.find(k) == nodes_with_ref.end() &&
                    reference_nodes.find(k) == reference_nodes.end()) {
                  reference_nodes[k] = refNodes[k];
                }
              }
            }
            done.emplace_back(cur);
          }
        }
      }  // End path search
    } else {
      // Save all reference nodes if full reference pathway was requested
      std::vector<std::string> v = this->config->reference_remove;
      for (const auto& p : refNodes) {
        // Check for nodes to remove, values can either be the name or kegg id
        // Save if not saved yet
        if (std::find(v.begin(), v.end(), p.first) == v.end() &&
            std::find(v.begin(), v.end(), p.second) == v.end() &&
            nodes_with_ref.find(p.first) == nodes_with_ref.end()) {
          reference_nodes[p.first] = p.second;
        }
      }
    }
    if (verbose) {
      std::cout << " done!" << std::endl << "Save referenece nodes/edges...";
    }
    // Save all edges for reference nodes
    for (const auto& v : refEdges) {
      std::string n1 = v[0];
      std::string n2 = v[1];
      // Save ref to ref edges
      if (reference_nodes.find(n1) != reference_nodes.end() &&
          reference_nodes.find(n2) != reference_nodes.end()) {
        ref_to_ref_edges.emplace_back(std::pair(n1, n2));
        // Save node to ref edges
      } else if ((nodes_with_ref.find(n1) != nodes_with_ref.end() &&
                  reference_nodes.find(n2) != reference_nodes.end())) {
        node_to_ref_edges.emplace_back(
            std::pair(nodes_with_ref[n1]->getId(), n2));
      } else if ((nodes_with_ref.find(n2) != nodes_with_ref.end() &&
                  reference_nodes.find(n1) != reference_nodes.end())) {
        node_to_ref_edges.emplace_back(
            std::pair(nodes_with_ref[n2]->getId(), n1));
      } else if ((nodes_with_ref.find(n1) != nodes_with_ref.end() &&
                  nodes_with_ref.find(n2) != nodes_with_ref.end())) {
        node_to_node_edges.emplace_back(std::pair(nodes_with_ref[n1]->getId(),
                                                  nodes_with_ref[n2]->getId()));
        ;
      }
    }
  }

  if (verbose) {
    std::cout << " done!" << std::endl
              << "Search targeted for reference nodes...";
  }
  if (!this->config->reference_files.empty()) {
    // Step 1: Search libraries for kegg_ids and create targeted library
    for (auto lib : identification_libs) {
      for (auto cmp : lib->getLibraryCompounds()) {
        std::string kegg_id = cmp->getFeature("PRECURSOR_KEGG_ID");
        if (kegg_id.empty()) {
          kegg_id = cmp->getFeature("MET_KEGG");
        }
        if (!kegg_id.empty() &&
            reference_nodes.find(kegg_id) != reference_nodes.end()) {
          cmp->addFeature("lib_source", lib->getFileName());
          cmp->addFeature("KEGG_ID", kegg_id);

          // Set quantification ions
          std::set<float> ions;
          for (auto frag : cmp->getFragments()) {
            for (int iIon = std::min(frag.first, frag.second);
                 iIon <= std::max(frag.first, frag.second); iIon++) {
              ions.insert(float(iIon));
            }
          }
          cmp->setQuantificationIons(ions);
          this->ref_nodes_lib.addCompound(*cmp);
        }
      }
    }

    // Step 2: Do the targeted search
    gcms::ReplicateMIDData* targeted_mids =
        searchTargetedMids(all_labeled_files, experiment_file_mapping,
                           &this->ref_nodes_lib, false);

    if (targeted_mids != nullptr) {
      // Step 3: Add nodes from reference
      const std::vector<std::string>& groups = targeted_mids->getGroupNames();
      std::vector<std::string> found_refs;
      for (auto mid_series : targeted_mids->getMIDCompoundSeries()) {
        std::map<std::string,
                 std::map<int, std::vector<std::pair<double, double>>>>
            ref_mids;
        std::string kegg_id =
            mid_series->getReferenceLibraryCompound().getFeature("KEGG_ID");
        int fragmentCount = mid_series->getFragmentCount();

        std::map<std::string,
                 std::map<int, std::vector<std::pair<double, double>>>>
            node_mids;

        int lastIon = 0;
        int ionCount = 0;
        // Only look for MIDs if no MIDs saved yet.
        if (std::find(found_refs.begin(), found_refs.end(), kegg_id) ==
            found_refs.end()) {
          for (int iIon = 0; iIon < mid_series->getIonCount(); ++iIon) {
            int ion = mid_series->getFragmentIon(iIon);
            if (ion != lastIon) {
              ionCount++;
              lastIon = ion;
            }
            for (const auto& group : groups) {
              double mi = targeted_mids->getGroupMeanMIAbundance(
                  group, *mid_series, iIon, true);
              double err = targeted_mids->getGroupStdErrorMIAbundance(
                  group, *mid_series, iIon, true);
              // Save the largest ion only
              if (ionCount == fragmentCount) {
                ref_mids[group][ion].push_back(std::make_pair(mi, err));
              }
            }
          }

          // Create real node and calculate variability
          auto n = new miami::Node(current_id, this->reference_nodes[kegg_id]);
          n->setFiles(targeted_mids->getGroupMemberCount(groups[0]), 0);
          n->setRef(true);
          n->setKegg(kegg_id);
          n->setMids(ref_mids);
          n->setReference(nullptr, -1.0,
                          mid_series->getReferenceLibraryCompound().getFeature(
                              "lib_source"));
          n->setRt(
              mid_series->getReferenceLibraryCompound().getRetentionTime());
          n->setRi(
              mid_series->getReferenceLibraryCompound().getRetentionIndex());

          n->findLargestCommonIon(this->config->miami_error_per_isotopomer);

          if (n->isValid()) {
            // Remove reference node and change reference edges if node is valid
            n->calculateVariability();
            this->reference_nodes.erase(kegg_id);
            for (int e = this->ref_to_ref_edges.size() - 1; e >= 0; e--) {
              if (this->ref_to_ref_edges[e].first == kegg_id) {
                this->node_to_ref_edges.emplace_back(
                    std::pair(current_id, this->ref_to_ref_edges[e].second));
                this->ref_to_ref_edges.erase(this->ref_to_ref_edges.begin() +
                                             e);
              } else if (this->ref_to_ref_edges[e].second == kegg_id) {
                this->node_to_ref_edges.emplace_back(
                    std::pair(current_id, this->ref_to_ref_edges[e].first));
                this->ref_to_ref_edges.erase(this->ref_to_ref_edges.begin() +
                                             e);
              }
            }

            for (int e = this->node_to_ref_edges.size() - 1; e >= 0; e--) {
              if (this->node_to_ref_edges[e].second == kegg_id) {
                this->node_to_node_edges.emplace_back(
                    std::pair(current_id, this->node_to_ref_edges[e].first));
                this->node_to_ref_edges.erase(this->node_to_ref_edges.begin() +
                                              e);
              }
            }

            this->nodes[current_id] = n;
            current_id++;
            found_refs.push_back(kegg_id);
          } else {
            delete n;
          }
        }
      }
    }
  }
  if (verbose) {
    std::cout << "done!" << std::endl;
  }
  this->done = true;
}

/**
 * @brief      Creates the Miami object from json configuration
 *
 * @param[in]  jsonImport  Configuration json object
 */
void Miami::fromJson(json jsonImport) {
  jsonNodes = jsonImport["nodes"];
  jsonEdges = jsonImport["edges"];

  json jsonConfig = jsonImport["config"];
  json jsonExperiments = jsonImport["experiments"];
  // Load config
  this->config->fromJson(jsonConfig);
  if (this->checkFiles().empty()) {
    // Load experiments
    for (auto exp : jsonExperiments) {
      addExperiment(exp["name"],
                    exp["labeled_files"].get<std::vector<std::string>>(),
                    exp["unlabeled_files"].get<std::vector<std::string>>());
    }
    // Load libraries
    for (const auto& lib :
         jsonConfig["libraries"].get<std::vector<std::string>>()) {
      this->loadLibrary(lib);
    }
  }
}

/**
 * @brief      Checks if files in a MIAMI experiment exists.
 *
 * @return     Vector of all missing files.
 */
std::vector<std::string> Miami::checkFiles() {
  // Check configuration files
  std::vector<std::string> not_found = this->config->checkFiles();

  // Check data files
  for (const auto& exp : this->experiments) {
    std::vector<std::string> exp_not_found = exp->checkFiles();
    not_found.insert(not_found.end(), exp_not_found.begin(),
                     exp_not_found.end());
  }

  return not_found;
}

/**
 * @brief      Converts the MIAMI object to a json object.
 *
 * @return     json object containing all parameters.
 */
fifo_json Miami::toJson() {
  // json jsonNodes;
  // json jsonEdges;
  json jsonConfig;
  json jsonExperiments;
  fifo_json jsonExport;

  for (auto experiment : this->experiments) {
    // Add experiment to json object
    jsonExperiments.push_back(experiment->toJson());
  }

  // Convert nodes and edges to json if no json representation saved
  if (jsonNodes.is_null() || jsonEdges.is_null()) {
    for (auto node : this->nodes) {
      jsonNodes.push_back(node.second->toJson());
    }
    for (auto experiment : this->experiments) {
      // Create edges for every node
      for (auto n1 : experiment->getDistances()) {
        int source = n1.first;
        for (auto n2 : n1.second) {
          int target = n2.first;
          double distance = n2.second;
          if (source != target) {
            jsonEdges.push_back({{"experiment", experiment->getName()},
                                 {"target", target},
                                 {"source", source},
                                 {"distance", distance}});
          }
        }
      }
    }

    if (!reference_nodes.empty() &&
        (!ref_to_ref_edges.empty() || !node_to_ref_edges.empty())) {
      for (const auto& p : ref_to_ref_edges) {
        jsonEdges.push_back({{"experiment", "Reference"},
                             {"target", p.second},
                             {"source", p.first},
                             {"distance", 0}});
      }
      for (const auto& p : node_to_ref_edges) {
        jsonEdges.push_back({{"experiment", "Reference"},
                             {"target", p.second},
                             {"source", p.first},
                             {"distance", 0}});
      }
      for (const auto& p : node_to_node_edges) {
        jsonEdges.push_back({{"experiment", "Reference"},
                             {"target", p.second},
                             {"source", p.first},
                             {"distance", 0}});
      }
      for (const auto& [kegg_id, name] : reference_nodes) {
        jsonNodes.push_back({{"id", kegg_id},
                             {"ion", -1},
                             {"kegg_id", kegg_id},
                             // {"library", this->config->reference_file},
                             {"mids", nullptr},
                             {"name", name},
                             {"ri", -1},
                             {"rt", -1},
                             {"score", 1},
                             {"is_ref", true},
                             {"variability", nullptr}});
      }
    }
  }

  // Configuration
  jsonConfig = this->config->toJson();

  jsonExport["config"] = jsonConfig;
  jsonExport["experiments"] = jsonExperiments;
  jsonExport["nodes"] = jsonNodes;
  jsonExport["edges"] = jsonEdges;

  return jsonExport;
}

/**
 * @brief      Saves the MIAMI object in json file.
 *
 * @param[in]  filename     The filename to save.
 */
void Miami::saveJson(const std::string& filename) {
  fifo_json jsonExport = this->toJson();
  std::ofstream output_stream(filename);
  output_stream << std::setw(2) << jsonExport << std::endl;
}

/**
 * @brief      Loads a MIAMI object from json file.
 *
 * @param[in]  filename     The filename to save.
 */
void Miami::loadJson(const std::string& filename) {
  std::ifstream input_stream(filename);
  json jsonConfig;
  input_stream >> jsonConfig;

  this->fromJson(jsonConfig);
}

/**
 * @brief      Converts the MIAMI object to a string in csv format.
 *
 * @return     Data in csv formt.
 */
std::string Miami::toCsv() {}

/**
 * @brief      Targeted search for MIDs.
 *
 * @param[in]  file_list     The list of all files to process
 * @param[in]  file_mapping  The file to experiment mapping
 * @param      library       The compound library
 *
 * @return     Pointer to replicate MID data object.
 */
gcms::ReplicateMIDData* Miami::searchTargetedMids(
    const std::vector<std::string>& file_list,
    const std::map<std::string, std::list<std::string>>& file_mapping,
    gcms::LibrarySearch<int, float>* library, bool lib_as_ref) {
  gcms::ReplicateResultSeries<int, float>* result =
      gcms::ReplicateResultSeries<int, float>::createReplicateResultSeries(
          library, file_list,
          this->config
              ->mid_identification_cutoff,  // Cutoff for targeted search
          false,                            // bool use_only_auto_ions=false
          true,                             // bool detect_quant_ions=true,
          false,   // bool delete_lib_on_destruction=false,
          false,   // bool filter=false,
          1,       // size_t min_ions=1,
          1000.0,  // peak_quality_cutoff
          5.0);    // double min_sn=50.0

  result->setReplicateGroupMapping(file_mapping);
  // Files the metabolite needs to be in, 1.0 for all files
  result->removeNonReproducableCompounds(0.0);

  std::string isotopes(
      "H\t1\t1.007825\t99.9844\nH\t2\t2.0141\t0.0156\nC\t12\t12\t98."
      "9184\nC\t13\t13.0034\t1.0816\nN\t14\t14.0031\t99.6337\nN\t15\t15."
      "0001\t0.3663\nO\t16\t15.9949\t99.758\nO\t17\t16.9991\t0."
      "038\nO\t18\t17.9992\t0.204\nSi\t28\t27.977\t92.22\nSi\t29\t28.976\t4."
      "69\nSi\t30\t29.974\t3.09\nP\t31\t30.974\t100\nS\t32\t31.972\t95."
      "039\nS\t33\t32.971\t0.749\nS\t34\t33.968\t4.197\nS\t36\t35.967\t0."
      "015");
  tools::NaturalMIDCalculator mid_calc(isotopes);

  gcms::ReplicateMIDData* mid_data = nullptr;

  for (auto ref : result->getAllReferenceCompounds()) {
    // int id = std::stoi(ref->getFeature("NODE_ID"));

    // gcms::LibraryCompound<int, float>* comp =
    // this->nodes[id]->getReference();

    gcms::MIDCompoundSeries* mid_series = new gcms::MIDCompoundSeries(
        *ref, result->getCompoundSeries(ref), mid_calc,
        std::string(config->tracer), lib_as_ref);

    if (mid_data == nullptr) {
      // Discard results with no samples, create new ReplicateMIDData
      // if first series.
      if (mid_series->getSampleCount() == 0) {
        continue;
      }
      mid_data = new gcms::ReplicateMIDData(mid_series);
    } else {
      mid_data->addMIDCompoundSeries(mid_series);
    }
  }

  if (mid_data != nullptr) {
    mid_data->setSampleNames(file_list);
    // set group mapping
    mid_data->setReplicateGroupMapping(result->getGroupMapping());
  }

  return mid_data;
}

gcms::ReplicateResultSeries<int, float>* Miami::searchTargetedQuant(
    const std::vector<std::string>& file_list,
    const std::map<std::string, std::list<std::string>>& file_mapping,
    gcms::LibrarySearch<int, float>* library) {
  // Use same settings as for MID determination
  gcms::ReplicateResultSeries<int, float>* result =
      gcms::ReplicateResultSeries<int, float>::createReplicateResultSeries(
          library, file_list,
          this->config
              ->mid_identification_cutoff,  // Cutoff for targeted search
          false,                            // bool use_only_auto_ions=false
          true,                             // bool detect_quant_ions=true,
          false,   // bool delete_lib_on_destruction=false,
          false,   // bool filter=false,
          1,       // size_t min_ions=1,
          1000.0,  // peak_quality_cutoff
          5.0);    // double min_sn=50.0

  result->setReplicateGroupMapping(file_mapping);
  // Files the metabolite needs to be in, 1.0 for all files
  result->removeNonReproducableCompounds(0.0);

  return result;
}

/**
 * @brief      Gets the internal metabolite library.
 *
 * @return     The library search with non-targeted detected metabolites.
 */
gcms::LibrarySearch<int, float>* Miami::getLibrary() {
  return this->internal_lib;
}

/**
 * @brief      Determines if analysis is done.
 *
 * @return     True if done, False otherwise.
 */
bool Miami::isDone() { return this->done; }

}  // namespace miami
