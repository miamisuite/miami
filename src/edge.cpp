/** @file edge.cpp
    @brief MIAMI edge object
    @copyright Copyright (C) 2019 Christian-Alexander Dudek

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "edge.hpp"
#include "config.hpp"

// External headers
#include <nlohmann/json.hpp>
using json = nlohmann::json;

namespace miami {

/**
 * @brief      Constructs the edge object.
 */
Edge::Edge(int source, int target) : source_id(source), target_id(target) {}

/**
 * @brief      Destroys the object.
 */
Edge::~Edge() = default;

void Edge::addExperiment(std::string experiment, double distance) {
  distances[experiment] = distance;
}

/**
 * @brief      Returns a json representation of the edge.
 *
 * @return     The edge json object.
 */
void Edge::fromJson(json jsonImport){

};

/**
 * @brief      Returns a json representation of the edge.
 *
 * @return     The edge json object.
 */
json Edge::toJson() {
  json jsonExport;
  jsonExport["distances"] = distances;
  jsonExport["source"] = source_id;
  jsonExport["target"] = target_id;

  return jsonExport;
};

/**
 * @brief      Returns a csv representation of the edge.
 *
 * @return     csv representation of the edge.
 */
std::string Edge::toString() {
  std::string out;
  return out;
};

}  // namespace miami
