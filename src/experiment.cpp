/** @file experiment.cpp
    @brief MIAMI experiment object
    @copyright Copyright (C) 2019 Christian-Alexander Dudek

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "experiment.hpp"

#include "config.hpp"
#include "node.hpp"
#include "tools.hpp"

#include <iostream>
#include <sstream>
#include <vector>

// Labid headers
#include "labelidentificator.h"

// External headers
#include <nlohmann/json.hpp>
using json = nlohmann::json;

namespace miami {

/**
 * @brief      Constructs the object.
 *
 * @param[in]  experiment_name  The experiment name
 * @param      nodes            The nodes
 * @param[in]  labeled_files    The labeled files
 * @param[in]  unlabeled_files  The unlabeled files
 */
Experiment::Experiment(std::string experiment_name, std::map<int, Node*>* nodes,
                       std::vector<std::string> labeled_files,
                       std::vector<std::string> unlabeled_files, Config* config)
    : name(std::move(experiment_name)),
      nodes(nodes),
      labeled_files(std::move(labeled_files)),
      unlabeled_files(std::move(unlabeled_files)),
      config(config) {}

/**
 * @brief      Destroys the object.
 */
Experiment::~Experiment() = default;

/**
 * @brief      Gets the labeled files.
 *
 * @return     The labeled files.
 */
std::vector<std::string> Experiment::getLabeledFiles() const {
  return this->labeled_files;
}

/**
 * @brief      Sets the experiment name.
 *
 * @param[in]  experiment_name    The experiment name
 */
void Experiment::setName(std::string experiment_name) {
  this->name = experiment_name;
}

/**
 * @brief      Sets the labeled files.
 *
 * @param[in]  files    The labeled files
 */
void Experiment::setLabeledFiles(std::vector<std::string> files) {
  this->labeled_files = files;
}

/**
 * @brief      Sets the unlabeled files.
 *
 * @param[in]  files    The unlabeled files
 */
void Experiment::setUnlabeledFiles(std::vector<std::string> files) {
  this->unlabeled_files = files;
}

std::vector<std::string> Experiment::checkFiles() {
  std::vector<std::string> not_found;
  std::ifstream f;

  for (const auto& file : this->labeled_files) {
    f = std::ifstream(file.c_str());
    if (!f.good()) {
      if (this->config->verbose) {
        std::cout << "Labeled file " << file << " not found!" << std::endl;
      }
      not_found.push_back(file);
    }
  }

  for (const auto& file : this->unlabeled_files) {
    f = std::ifstream(file.c_str());
    if (!f.good()) {
      if (this->config->verbose) {
        std::cout << "Unlabeled file " << file << " not found!" << std::endl;
      }
      not_found.push_back(file);
    }
  }

  return not_found;
}

/**
 * @brief      Gets the unlabeled files.
 *
 * @return     The unlabeled files.
 */
std::vector<std::string> Experiment::getUnlabeledFiles() const {
  return this->unlabeled_files;
}

int Experiment::numLabeledFiles() const { return this->labeled_files.size(); }
int Experiment::numUnlabeledFiles() const {
  return this->unlabeled_files.size();
}
/**
 * @brief      Gets the name.
 *
 * @return     The name.
 */
std::string Experiment::getName() const { return this->name; }

/**
 * @brief      Gets the labeled metabolites.
 *
 * @return     The labeled metabolites.
 */
std::vector<labid::LabeledCompound*> Experiment::getLabeledMetabolites() const {
  return this->labeled_metabolites;
}

/**
 * @brief      Gets all metabolites.
 *
 * @return     All metabolites.
 */
std::vector<gcms::LibraryCompound<int, float>*> Experiment::getAllMetabolites()
    const {
  return this->all_metabolites;
}

/**
 * @brief      Starts label detection.
 */
void Experiment::detectLabeledMetabolites() {
  // Load files
  auto lid = new labid::LabelIdentificator(unlabeled_files, labeled_files);

  // MIA labid settings (see mia/labelingdataset.cpp)
  lid->setMaximumLabel(
      this->config->labid_max_labeling);  // based on % labeled substrate!
  lid->setEnsureM0PresenceInLabeledSpec(true);
  lid->setEnsureM1LessThanM0(false);
  lid->setFragmentDetectionPlateauTolerance(1.1);
  lid->setMaxMMinusOne(0.25);
  lid->setMinimumM1(0.01);
  lid->setFilterByConfidenceInterval(true);
  lid->setMinimalSN(5);
  lid->setRequiredSpectrumFrequency(
      this->config
          ->labid_required_replicates);  // % Replicates the metabolite needs
                                         // to be in 1.0 for all replicates

  std::list<std::pair<size_t, size_t>> filter;
  filter.emplace_back(std::pair<size_t, size_t>(0, 80));
  filter.emplace_back(std::pair<size_t, size_t>(147, 147));
  lid->setMassFilter(filter);

  lid->setRequiredLabelAmount(
      this->config->labid_min_labeling);  // Percent minimum labeling
  lid->setRequiredR2(
      this->config->labid_min_midsolv_r2);  // Minimum R2 from MID determination
  lid->setMinimalNumberOfFragments(2);
  lid->setMaximalFragmentDeviation(
      this->config
          ->labid_dev_abs_sum);  // +- deviation from 1.0 in sum(abs(M_i))
  lid->setCorrectionRatio(0.010934);

  // Find labeled compounds
  lid->startAnalysis();

  this->labeled_metabolites = lid->getLabeledCompounds();

  // Sort metabolites
  sort(this->labeled_metabolites.begin(), this->labeled_metabolites.end(),
       [](const auto& lhs, const auto& rhs) {
         return lhs->getName() < rhs->getName();
       });

  delete lid;
}

/**
 * @brief      Calculates the distances.
 */
void Experiment::calculateDistances() {
  for (auto n : (*this->nodes)) {
    for (auto m : (*this->nodes)) {
      int node_id1 = std::min(n.first, m.first);
      int node_id2 = std::max(n.first, m.first);

      // Skip same nodes
      if (node_id1 == node_id2) {
        this->distance_matrix[node_id1][node_id2] = 0.0;
        continue;
      }
      // Skip if we already have a value
      if (this->distance_matrix.find(node_id1) != this->distance_matrix.end() &&
          this->distance_matrix[node_id1].find(node_id2) !=
              this->distance_matrix[node_id1].end()) {
        continue;
      }

      miami::Node* pNode1 = (*this->nodes)[node_id1];
      miami::Node* pNode2 = (*this->nodes)[node_id2];

      std::vector<std::pair<double, double>> mid1 =
          pNode1->getMids(this->getName(), pNode1->getCommonIon());
      std::vector<std::pair<double, double>> mid2 =
          pNode2->getMids(this->getName(), pNode2->getCommonIon());

      // If the sum of MIDs equals 0, use maximum distance,
      // because we did not this metabolite.
      float sum1 = pNode1->getMidSum(this->getName(), pNode1->getCommonIon());
      float sum2 = pNode2->getMidSum(this->getName(), pNode2->getCommonIon());

      if (sum1 == 0.0 || sum2 == 0.0) {
        this->distance_matrix[node_id1][node_id2] = 1.0;
        continue;
      }

      this->distance_matrix[node_id1][node_id2] =
          miami_tools::distance(mid1, mid2, this->config->miami_distance_method,
                                this->config->miami_distance_norm);
    }
  }
}

/**
 * @brief      Gets the distances.
 *
 * @return     The distances.
 */
std::map<int, std::map<int, double>> Experiment::getDistances() {
  return this->distance_matrix;
};

/**
 * @brief      Gets the nodes.
 *
 * @return     The nodes.
 */
std::map<int, Node*>* Experiment::getNodes() const { return this->nodes; }

// Node* Experiment::getNode(int node_id)
// {
//     return this->nodes[node_id]; // TODO (cdu): Check if node exists;
// }

// void Experiment::addNode(int node_id, Node* node)
// {
//     debug(this->getName() << ": Adding node " << node_id)
//     this->nodes[node_id] = node;
// }

// int Experiment::delNode(int node_id)
// {
//     debug(this->getName() << ": Deleting node " << node_id)
//     this->nodes.erase(node_id);
//     return this->nodes.size();
// }

/**
 * @brief      Loads expriment from json object.
 *
 * @param[in]  jsonImport  Experiment json object
 */
void Experiment::fromJson(json jsonImport) {
  this->name = jsonImport["name"];
  this->labeled_files =
      jsonImport["labeled_files"].get<std::vector<std::string>>();
  this->unlabeled_files =
      jsonImport["unlabeled_files"].get<std::vector<std::string>>();
}

/**
 * @brief      Converts the Experiment object to a json object.
 *
 * @return     json object containing all experiment values.
 */
json Experiment::toJson() {
  json jsonExport;

  jsonExport["name"] = this->getName();
  jsonExport["labeled_files"] = this->getLabeledFiles();
  jsonExport["unlabeled_files"] = this->getUnlabeledFiles();

  return jsonExport;
}

/**
 * @brief      Returns a string representation of the object.
 *
 * @return     String representation of the object.
 */
std::string Experiment::toString() {
  std::stringstream ss;
  ss << "Experiment \"" << this->getName() << "\" (";
  if (!this->getLabeledMetabolites().empty()) {
    ss << this->getLabeledMetabolites().size() << " labeled metabolites";
  } else {
    ss << "no labeled metabolites";
  }

  ss << ")";

  return ss.str();
}
}  // namespace miami
