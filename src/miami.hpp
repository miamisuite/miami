/** @file miami.hpp
    @brief MIAMI object
    @copyright Copyright (C) 2019 Christian-Alexander Dudek

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MIAMI_H
#define MIAMI_H

#include "config.hpp"
#include "experiment.hpp"
#include "node.hpp"

#include <chrono>
#include <string>
#include <vector>

// GCMS headers
#include "labelidentificator.h"
#include "librarysearch.h"
#include "replicatemiddata.h"
#include "replicateresultseries.h"
#include "sqllibrary.h"

// External headers
#include <nlohmann/fifo_map.hpp>
#include <nlohmann/json.hpp>
using json = nlohmann::json;

// A workaround to give to use fifo_map as map, we are just ignoring the
// 'less' compare
template <class K, class V, class dummy_compare, class A>
using unsorted_fifo_map =
    nlohmann::fifo_map<K, V, nlohmann::fifo_map_compare<K>, A>;
using fifo_json = nlohmann::basic_json<unsorted_fifo_map>;

namespace miami {

/**
 * @brief      Class for miami.
 */
class Miami {
 public:
  Miami(Config* config_);
  ~Miami();

  fifo_json toJson();
  void fromJson(json jsonImport);
  void saveJson(const std::string& filename);
  void loadJson(const std::string& filename);
  std::string toCsv();

  bool addExperiment(const std::string& experiment_name,
                     const std::vector<std::string>& labeled_files,
                     const std::vector<std::string>& unlabeled_files);
  void changeExperiment(const int id, const std::string& experiment_name,
                        const std::vector<std::string>& labeled_files,
                        const std::vector<std::string>& unlabeled_files);
  void removeExperiment(const std::string& name);
  void removeExperiment(const int id);
  std::vector<miami::Experiment*> getExperiments();
  bool loadLibrary(const std::string& library_filename);
  void removeLibrary(const std::string& library_filename);
  void printLibraries();
  void saveLibrary(const std::string& library_filename);
  std::vector<std::string> checkFiles();
  void run();
  bool isDone();
  gcms::LibrarySearch<int, float>* getLibrary();

 private:
  bool done = false;  /**< Indicates if the analysis was done */
  int current_id = 0; /**< The largest node id */
  Config* config;     /**< Configuration instance */
  std::vector<std::string> library_files; /**< A vector with strings containing
                                             the library file path */
  std::vector<gcms::LibrarySearch<int, float>*>
      identification_libs; /**< A vector with pointer to LibrarySearch objects
                            */
  gcms::SQLLibrary* nist_lib; /**< The nist library as SQLite library */
  std::vector<miami::Experiment*>
      experiments; /**< A vector with pointer to experiment objects */
  std::map<int, miami::Node*>
      nodes; /**< A mapping of node ids and pointer to node objects */

  labid::LabelIdentificator* lid; /**< Pointer to the LabelIdentificator object
                                     for NTFD label detection */
  gcms::LibrarySearch<int, float>
      ref_nodes_lib; /**< The LibrarySearch object with metabolites from
                        reference pathway for additional targeted search */

  gcms::LibrarySearch<int, float>*
      internal_lib; /**< The LibrarySearch object with non-targeted detected
                      metabolites */
  json jsonEdges; /**< All edges in json format */
  json jsonNodes; /**< All nodes in json format */
  bool export_msl = true;
  bool export_lbr = true;
  std::map<std::string, std::string> reference_nodes;

  gcms::ReplicateMIDData* searchTargetedMids(
      const std::vector<std::string>& file_list,
      const std::map<std::string, std::list<std::string>>& file_mapping,
      gcms::LibrarySearch<int, float>* library, bool lib_as_ref);

  gcms::ReplicateResultSeries<int, float>* searchTargetedQuant(
      const std::vector<std::string>& file_list,
      const std::map<std::string, std::list<std::string>>& file_mapping,
      gcms::LibrarySearch<int, float>* library);

  std::vector<std::pair<std::string, std::string>> ref_to_ref_edges;
  std::vector<std::pair<int, int>> node_to_node_edges;
  std::vector<std::pair<int, std::string>> node_to_ref_edges;
};
}  // namespace miami

#endif  // MIAMI_H
